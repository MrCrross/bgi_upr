<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Tracker Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used across application for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'created' => 'Создана :model :label',

    'updating' => 'Изменение :model :label',

    'deleting' => 'Удаление :model :label',

    'restored' => 'Восстановлен :model :label',

    //you may add your own model name language line here
    'models' => [
        'author' => 'автор',
        'genre' => 'жанр',
        'news' => 'новость',
        'files' => 'файл',
        'order' => 'заказ',
        'orders_product' => 'товары заказа',
        'product' => 'товар',
        'publisher' => 'издатель',
        'user' => 'пользователь',
    ],
];
