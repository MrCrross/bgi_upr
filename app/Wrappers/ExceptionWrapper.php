<?php

namespace App\Wrappers;

use App\Traits\ResponseTrait;
use Exception;
use Illuminate\Http\JsonResponse;
use Throwable;

abstract class ExceptionWrapper extends Exception
{
    use ResponseTrait;
    /**
     * @var array
     */
    protected static array $errors = [];
    private static array $defaultErrors = [
        'default' => 'Произошла ошибка... Обратитесь к Администратору!'
    ];
    protected static bool $debug = false;
    protected static array $params = [];

    public function __construct(string $message = "", int $code = 0, ?Throwable $previous = null, array $params = [], bool $debug = false)
    {
        parent::__construct($message, $code, $previous);
        self::$params = $params;
        self::$debug = $debug;
    }

    /**
     * Заполняет статический массив $errors
     * @return array
     */
    abstract protected function setErrors(): array;

    /**
     * @return JsonResponse
     */
    public function render(): JsonResponse
    {
        if (env('APP_DEBUG')) {
            self::$errors = $this->setErrors();
            if (!isset(self::$errors[$this->getMessage()])) {
                $error = $this->getMessage();
            } else {
                $error = self::$errors[$this->getMessage()];
            }
        } else {
            $error = self::$defaultErrors['default'];
        }
        if (self::$debug && !env('APP_DEBUG')) {
            self::$params = [];
        }

        return response()->json(self::sendResponse(message: $error, params: self::$params), 400);
    }

}
