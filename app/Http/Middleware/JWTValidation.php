<?php

namespace App\Http\Middleware;

use App\Exceptions\CoreException;
use Closure;
use Illuminate\Http\Request;
use ReallySimpleJWT\Token;
use Symfony\Component\HttpFoundation\Response;

class JWTValidation
{
    public static ?array $tokenData = [];
    public static ?string $token = '';
    public static bool $tokenIsValid = false;

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param \Closure(Request): (Response) $next
     * @return Response
     * @throws CoreException
     */
    public function handle(Request $request, Closure $next): Response
    {
        self::$token = $request->header('Authorization');

        if (!empty(self::$token)) {
            self::$tokenIsValid = Token::validate(self::$token, env('SECRET_JWT'));
        }

        if (self::$tokenIsValid) {
            self::$tokenData = Token::getPayload(self::$token);
        } else {
            throw new CoreException('no_auth');
        }

        return $next($request);
    }
}
