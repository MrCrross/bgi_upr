<?php

namespace App\Http\Middleware;

use App\Exceptions\CoreException;
use App\Models\Users\UsersAuthorization;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class UserAuthorizationValidation
{
    /**
     * Handle an incoming request.
     *
     * @param \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response) $next
     * @throws CoreException
     */
    public function handle(Request $request, Closure $next): Response
    {
        $userTokenData = JWTValidation::$tokenData;
        $tokenIsValid = JWTValidation::$tokenIsValid;
        $token = JWTValidation::$token;

        if (!empty($userTokenData) && $tokenIsValid && isset($token)) {
            $activeUserSession = UsersAuthorization::activeSession($userTokenData['user_id'], $token);

            if (!empty($activeUserSession->id)) {
                Auth::loginUsingId($userTokenData['user_id']);
                UsersAuthorization::query()
                    ->where('id', '=', $activeUserSession->id)
                    ->update(['reload_session' => false]);
            } else {
                throw new CoreException('no_auth');
            }
        } else {
            throw new CoreException('no_auth');
        }

        return $next($request);
    }
}
