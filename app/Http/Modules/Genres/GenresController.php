<?php

namespace App\Http\Modules\Genres;

use App\Exceptions\CoreException;
use App\Http\Controllers\Controller;
use App\Models\Genre;
use App\Traits\FilterTrait;
use App\Traits\OrderTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;

class GenresController extends Controller
{
    use FilterTrait;
    use OrderTrait;

    public static array $orderFields = [
        'name',
    ];

    public static array $filterFields = [
        'name' => [
            'type' => '',
            'action' => 'like'
        ],
        'status' => [
            'type' => '',
            'action' => '='
        ],
    ];

    public function __construct()
    {
        $this->middleware('permission:genres_view', ['only' => ['index']]);
        $this->middleware('permission:genres_edit', ['only' => ['create', 'store', 'edit', 'update', 'destroy']]);
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request): Response
    {
        self::setDefaultOrder(['id' => 'DESC']);
        $genres = Genre::query();
        $genres = self::filterData($request, $genres);
        $genres = self::orderData($request, $genres);
        $genres = $genres->paginate(6);

        return response()->view('genres.index', [
            'genres' => $genres,
            'order' => self::orderGenerate($request),
            'filter' => self::filterGenerate($request),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(): Response
    {
        return response()->view('genres.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'name' => ['required', 'string', Rule::unique(Genre::class, 'name')],
        ]);
        $fields = [
            'name' => $request->post('name')
        ];
        Genre::create($fields);

        return redirect()->route('genres.index')
            ->with('success', __('genres.messages.store'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     * @throws CoreException
     */
    public function show(int $id): Response
    {
        $genre = Genre::query()->find($id);
        if (empty($genre)) {
            throw new CoreException('no_object');
        }
        $history = $genre->getHistory();

        return response()->view('genres.show', [
            'genre' => $genre,
            'history' => $history,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit(int $id): Response
    {
        $genre = Genre::find($id);

        return response()->view('genres.edit', [
            'genre' => $genre,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        $request->validate([
            'name' => ['required', 'string', Rule::unique(Genre::class, 'name')->ignore($id)],
        ]);

        $fields = [
            'name' => $request->post('name'),
        ];
        Genre::find($id)->update($fields);

        return redirect()->route('genres.index')
            ->with('success', __('genres.messages.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        Genre::find($id)->update([
            'status' => false
        ]);

        return redirect()->route('genres.index')
            ->with('success', __('genres.messages.delete'));
    }

    /**
     * @param int $id
     * @return RedirectResponse
     */
    public function recovery(int $id): RedirectResponse
    {
        Genre::find($id)->update([
            'status' => true
        ]);

        return redirect()->route('genres.show', $id)
            ->with('success', __('genres.messages.recovery'));
    }

    public function autocomplete(): JsonResponse
    {
        return response()->json(Genre::autocomplete());
    }
}
