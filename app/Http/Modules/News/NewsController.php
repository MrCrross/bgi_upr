<?php

namespace App\Http\Modules\News;

use App\Exceptions\CoreException;
use App\Http\Controllers\Controller;
use App\Http\Modules\News\Requests\NewsStoreRequest;
use App\Models\News;
use App\Traits\FilterTrait;
use App\Traits\OrderTrait;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class NewsController extends Controller
{
    use FilterTrait;
    use OrderTrait;

    public static array $orderFields = [
        'date',
        'title',
        'status',
    ];

    public static array $filterFields = [
        'min_date' => [
            'type' => '',
            'action' => '>='
        ],
        'max_date' => [
            'type' => '',
            'action' => '<='
        ],
        'title' => [
            'type' => '',
            'action' => 'like'
        ],
        'status' => [
            'type' => '',
            'action' => '='
        ],
        'creator_id' => [
            'type' => '',
            'action' => '='
        ]
    ];

    public function __construct()
    {
        $this->middleware('permission:news_edit', ['only' => ['store', 'adminCreate', 'adminEdit', 'update', 'destroy', 'recovery', 'adminDestroy', 'adminRecovery']]);
    }

    public function adminIndex(Request $request): Response
    {
        self::setDefaultOrder(['id' => 'DESC']);
        $news = News::query()->with('creator');
        $news = self::filterData($request, $news);
        $news = self::orderData($request, $news);
        $news = $news
            ->paginate(10);

        return response()->view('news.index', [
            'news' => $news,
            'news_order' => self::orderGenerate($request),
            'news_filter' => self::filterGenerate($request),
        ]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): JsonResponse
    {
        self::setDefaultOrder(['id' => 'DESC']);
        $news = News::query()->with('creator');
        $news = self::filterData($request, $news);
        $news = self::orderData($request, $news);
        $news = $news
            ->where('status', '=', true)
            ->paginate(6);

        return response()->json([
            'news' => $news,
        ]);
    }

    public function adminCreate(): Response
    {
        return response()->view('news.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(NewsStoreRequest $request): JsonResponse
    {
        $fields = [
            'date' => Carbon::now()->toDateTimeString(),
            'title' => $request->post('title'),
            'content' => $request->post('content'),
            'creator_id' => Auth::id(),
        ];

        $news = News::query()->create($fields);

        if ($request->hasFile('image')) {
            $path = "/news/{$news->id}/";
            $fileName = "img.{$request->file('image')->clientExtension()}";
            Storage::putFileAs('/upload/' . $path, $request->file('image'), $fileName);
            $news->update(['image' => '/storage/upload/' . trim($path, '/') . '/' . trim($fileName, '/')]);
        }

        return response()->json([
           'news' => News::query()->with('creator')->find($news->id),
        ]);
    }

    public function adminStore(NewsStoreRequest $request): RedirectResponse
    {
        $fields = [
            'date' => Carbon::now()->toDateTimeString(),
            'title' => $request->post('title'),
            'content' => $request->post('content'),
            'creator_id' => Auth::id(),
        ];

        $news = News::query()->create($fields);

        if ($request->hasFile('image')) {
            $path = "/news/{$news->id}/";
            $fileName = "img.{$request->file('image')->clientExtension()}";
            Storage::putFileAs('/upload/' . $path, $request->file('image'), $fileName);
            $news->update(['image' => '/storage/upload/' . trim($path, '/') . '/' . trim($fileName, '/')]);
        }

        return Redirect::route('news.admin.create')->with('status', 'news-created');
    }

    /**
     * Display the specified resource.
     * @throws CoreException
     */
    public function show(int $id): JsonResponse
    {
        $news = News::query()->with('creator')->find($id);
        if (empty($news)) {
            throw new CoreException('no_object');
        }

        return response()->json($news);
    }

    /**
     * Display the specified resource.
     * @throws CoreException
     */
    public function adminShow(int $id): Response
    {
        $news = News::query()->with('creator')->find($id);
        $history = $news->getHistory();

        return response()->view('news.show', [
            'news' => $news,
            'history' => $history,
        ]);
    }

    public function adminEdit(int $id): Response
    {
        $news = News::query()->with('creator')->find($id);

        return response()->view('news.edit', [
            'news' => $news,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(NewsStoreRequest $request, int $id): JsonResponse
    {
        $fields = [
            'title' => $request->post('title'),
            'content' => $request->post('content'),
        ];

        $news = News::query()->updateOrCreate(['id' => $id], $fields);

        if ($request->hasFile('image')) {
            $path = "/news/{$news->id}/";
            $fileName = "img.{$request->file('image')->clientExtension()}";
            Storage::putFileAs('/upload/' . $path, $request->file('image'), $fileName);
            $news->update(['image' => '/storage/upload/' . trim($path, '/') . '/' . trim($fileName, '/')]);
        }

        return response()->json([
            'news' => News::query()->with('creator')->find($news->id),
        ]);
    }

    public function adminUpdate(NewsStoreRequest $request, int $id): RedirectResponse
    {
        $fields = [
            'title' => $request->post('title'),
            'content' => $request->post('content'),
        ];

        $news = News::query()->find($id);
        $news->update($fields);

        if ($request->hasFile('image')) {
            $path = "/news/{$news->id}/";
            $fileName = "img.{$request->file('image')->clientExtension()}";
            Storage::putFileAs('/upload/' . $path, $request->file('image'), $fileName);
            $news->update(['image' => '/storage/upload/' . trim($path, '/') . '/' . trim($fileName, '/')]);
        }

        return Redirect::route('news.admin.edit', $id)->with('status', 'news-updated');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id): void
    {
        News::query()->findOrFail($id)->update([
            'status' => false
        ]);
    }

    public function recovery(int $id): void
    {
        News::query()->findOrFail($id)->update([
            'status' => true
        ]);
    }

    public function adminDestroy(int $id): RedirectResponse
    {
        News::query()->findOrFail($id)->update([
            'status' => false
        ]);

        return redirect()->route('news.admin.index');
    }

    public function adminRecovery(int $id): RedirectResponse
    {
        News::query()->findOrFail($id)->update([
            'status' => true
        ]);

        return redirect()->route('news.admin.show', $id);
    }
}
