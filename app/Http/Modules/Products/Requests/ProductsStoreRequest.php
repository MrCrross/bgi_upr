<?php

namespace App\Http\Modules\Products\Requests;

use App\Traits\FormRequestValidationTrait;
use Illuminate\Foundation\Http\FormRequest;

class ProductsStoreRequest extends FormRequest
{
    use FormRequestValidationTrait;

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'isbn' => 'required|string',
            'price' => 'required|numeric',
            'description' => 'required|string',
            'image' => 'nullable|image',
            'name' => 'required|string',
            'pages' => 'required|integer',
            'year_published' => 'required|integer',
            'publisher_id' => 'required|integer|exists:publishers,id',
            'authors' => 'required|array',
            'authors.*' => 'required|integer|exists:authors,id',
            'genres' => 'required|array',
            'genres.*' => 'required|integer|exists:genres,id',
        ];
    }
}
