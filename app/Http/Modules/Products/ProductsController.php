<?php

namespace App\Http\Modules\Products;

use App\Exceptions\CoreException;
use App\Http\Controllers\Controller;
use App\Http\Modules\Products\Requests\ProductsStoreRequest;
use App\Models\Author;
use App\Models\Genre;
use App\Models\Product;
use App\Models\ProductsAuthor;
use App\Models\ProductsGenre;
use App\Models\Publisher;
use App\Traits\FilterTrait;
use App\Traits\OrderTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class ProductsController extends Controller
{
    use FilterTrait;
    use OrderTrait;

    public static array $orderFields = [
        'name',
        'price',
        'publisher_id',
    ];

    public static array $filterFields = [
        'min_date' => [
            'type' => '',
            'action' => '>='
        ],
        'max_date' => [
            'type' => '',
            'action' => '<='
        ],
        'min_price' => [
            'type' => '',
            'action' => '>='
        ],
        'max_price' => [
            'type' => '',
            'action' => '<='
        ],
        'title' => [
            'type' => '',
            'action' => 'like'
        ],
        'status' => [
            'type' => '',
            'action' => '='
        ],
        'publisher_id' => [
            'type' => '',
            'action' => '='
        ],
        'name' => [
            'type' => '',
            'action' => 'like',
        ],
    ];

    public function adminIndex(Request $request): Response
    {
        $products = Product::query()->with(['publisher', 'authors', 'genres']);
        $products = self::filterData($request, $products);
        $products = self::orderData($request, $products);
        $products = $products
            ->whereHas('authors', function ($query) use ($request) {
                $query->when($request->filled('authors'), function ($where) use ($request) {
                    $where->whereIn('authors.id', $request->query('authors'));
                });
            })
            ->whereHas('genres', function ($query) use ($request) {
                $query->when($request->filled('genres'), function ($where) use ($request) {
                    $where->whereIn('genres.id', $request->query('genres'));
                });
            })
        ;
        $products = $products->paginate(10);

        return response()->view('products.index', [
            'products' => $products,
            'products_order' => self::orderGenerate($request),
            'products_filter' => self::filterGenerate($request),
            'publisher_select' => Publisher::select()
        ]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): JsonResponse
    {
        $products = Product::query()->with(['publisher', 'authors', 'genres']);
        $products = self::filterData($request, $products);
        $products = $products
            ->whereHas('authors', function ($query) use ($request) {
                $query->when($request->filled('authors'), function ($where) use ($request) {
                    $where->whereIn('authors.id', $request->query('authors'));
                });
            })
            ->whereHas('genres', function ($query) use ($request) {
                $query->when($request->filled('genres'), function ($where) use ($request) {
                    $where->whereIn('genres.id', $request->query('genres'));
                });
            })
            ->where('status', '=', true)
        ;
        $products = $products->paginate(6);

        return response()->json([
            'products' => $products,
        ]);
    }

    public function adminCreate(): Response
    {
        $authors = Author::query()->select('id as value', 'fio as label')->get();
        $genres = Genre::query()->select('id as value', 'name as label')->get();
        $publishers = Publisher::query()->select('id as value', 'name as label')->get();

        return response()->view('products.create', [
            'authors' => $authors,
            'genres' => $genres,
            'publishers' => $publishers,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ProductsStoreRequest $request): JsonResponse
    {
        $fields = [
            'isbn' => $request->post('isbn'),
            'price' => $request->post('price'),
            'description' => $request->post('description'),
            'name' => $request->post('name'),
            'pages' => $request->post('pages'),
            'publisher_id' => $request->post('publisher_id'),
            'year_published' => $request->post('year_published'),
        ];

        $product = Product::query()->create($fields);
        if ($request->has('authors')) {
            foreach ($request->post('authors') as $author) {
                if ((int)$author !== 0) {
                    ProductsAuthor::query()->insert([
                        'product_id' => $product->id,
                        'author_id' => $author,
                    ]);
                }
            }
        }
        if ($request->has('genres')) {
            foreach ($request->post('genres') as $genre) {
                if ((int)$genre !== 0) {
                    ProductsGenre::query()->insert([
                        'product_id' => $product->id,
                        'genre_id' => $genre,
                    ]);
                }
            }
        }
        if ($request->hasFile('image')) {
            $path = "/products/{$product->id}/";
            $fileName = "img.{$request->file('image')->clientExtension()}";
            Storage::putFileAs('/upload/' . $path, $request->file('image'), $fileName);
            $product->update(['image' => '/storage/upload/' . trim($path, '/') . '/' . trim($fileName, '/')]);
        }

        return response()->json([
            'products' => Product::query()->with(['publisher', 'authors', 'genres'])->find($product->id),
        ]);
    }

    public function adminStore(ProductsStoreRequest $request): RedirectResponse
    {
        $fields = [
            'isbn' => $request->post('isbn'),
            'price' => $request->post('price'),
            'description' => $request->post('description'),
            'name' => $request->post('name'),
            'pages' => $request->post('pages'),
            'publisher_id' => $request->post('publisher_id'),
            'year_published' => $request->post('year_published'),
        ];

        $product = Product::query()->create($fields);
        if ($request->has('authors')) {
            $authors = [];
            foreach ($request->post('authors') as $author) {
                if ((int)$author !== 0 && !in_array($author, $authors)) {
                    ProductsAuthor::query()->insert([
                        'product_id' => $product->id,
                        'author_id' => $author,
                    ]);
                    $authors[] = $author;
                }
            }
        }
        if ($request->has('genres')) {
            $genres = [];
            foreach ($request->post('genres') as $genre) {
                if ((int)$genre !== 0 && !in_array($genre, $genres)) {
                    ProductsGenre::query()->insert([
                        'product_id' => $product->id,
                        'genre_id' => $genre,
                    ]);
                    $genres[] = $genre;
                }
            }
        }
        if ($request->hasFile('image')) {
            $path = "/products/{$product->id}/";
            $fileName = "img.{$request->file('image')->clientExtension()}";
            Storage::putFileAs('/upload/' . $path, $request->file('image'), $fileName);
            $product->update(['image' => '/storage/upload/' . trim($path, '/') . '/' . trim($fileName, '/')]);
        }

        return Redirect::route('products.admin.create')->with('status', 'product-created');
    }

    /**
     * Display the specified resource.
     * @throws CoreException
     */
    public function show(int $id): JsonResponse
    {
        $product = Product::query()->with(['publisher', 'authors', 'genres'])->find($id);
        if (empty($product)) {
            throw new CoreException('no_object');
        }
        $history = $product->getHistory();

        return response()->json([
            'product' => $product,
            'history' => $history,
        ]);
    }

    public function adminShow(int $id): Response
    {
        $product = Product::query()->with(['publisher', 'authors', 'genres'])->find($id);
        $history = $product->getHistory();

        return response()->view('products.show', [
            'product' => $product,
            'history' => $history,
        ]);
    }

    public function adminEdit(int $id): Response
    {
        $product = Product::query()->with(['publisher', 'authors', 'genres'])->find($id);
        $authors = Author::query()->select('id as value', 'fio as label')->get();
        $genres = Genre::query()->select('id as value', 'name as label')->get();
        $publishers = Publisher::query()->select('id as value', 'name as label')->get();

        return response()->view('products.edit', [
            'product' => $product,
            'authors' => $authors,
            'genres' => $genres,
            'publishers' => $publishers,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ProductsStoreRequest $request, int $id): JsonResponse
    {
        $fields = [
            'isbn' => $request->post('isbn'),
            'price' => $request->post('price'),
            'description' => $request->post('description'),
            'name' => $request->post('name'),
            'pages' => $request->post('pages'),
            'publisher_id' => $request->post('publisher_id'),
            'year_published' => $request->post('year_published'),
        ];

        $product = Product::query()->updateOrCreate(['id' => $id], $fields);
        if ($request->has('authors')) {
            ProductsAuthor::query()->where('product_id', '=', $product->id)->delete();
            foreach ($request->post('authors') as $author) {
                if ((int)$author !== 0) {
                    ProductsAuthor::query()->insert([
                        'product_id' => $product->id,
                        'author_id' => $author,
                    ]);
                }
            }
        }
        if ($request->has('genres')) {
            ProductsGenre::query()->where('product_id', '=', $product->id)->delete();
            foreach ($request->post('genres') as $genre) {
                if ((int)$genre !== 0) {
                    ProductsGenre::query()->insert([
                        'product_id' => $product->id,
                        'genre_id' => $genre,
                    ]);
                }
            }
        }
        if ($request->hasFile('image')) {
            $path = "/products/{$product->id}/";
            $fileName = "img.{$request->file('image')->clientExtension()}";
            Storage::putFileAs('/upload/' . $path, $request->file('image'), $fileName);
            $product->update(['image' => '/storage/upload/' . trim($path, '/') . '/' . trim($fileName, '/')]);
        }

        return response()->json([
            'products' => Product::query()->with(['publisher', 'authors', 'genres'])->find($product->id),
        ]);
    }

    public function adminUpdate(ProductsStoreRequest $request, int $id): RedirectResponse
    {
        $fields = [
            'isbn' => $request->post('isbn'),
            'price' => $request->post('price'),
            'description' => $request->post('description'),
            'name' => $request->post('name'),
            'pages' => $request->post('pages'),
            'publisher_id' => $request->post('publisher_id'),
            'year_published' => $request->post('year_published'),
        ];

        $product = Product::query()->find($id);
        $product->update($fields);
        if ($request->has('authors')) {
            ProductsAuthor::query()->where('product_id', '=', $product->id)->delete();
            $authors = [];
            foreach ($request->post('authors') as $author) {
                if ((int)$author !== 0 && !in_array($author, $authors)) {
                    ProductsAuthor::query()->insert([
                        'product_id' => $product->id,
                        'author_id' => $author,
                    ]);
                    $authors[] = $author;
                }
            }
        }
        if ($request->has('genres')) {
            ProductsGenre::query()->where('product_id', '=', $product->id)->delete();
            $genres = [];
            foreach ($request->post('genres') as $genre) {
                if ((int)$genre !== 0 && !in_array($genre, $genres)) {
                    ProductsGenre::query()->insert([
                        'product_id' => $product->id,
                        'genre_id' => $genre,
                    ]);
                    $genres[] = $genre;
                }
            }
        }
        if ($request->hasFile('image')) {
            $path = "/products/{$product->id}/";
            $fileName = "img.{$request->file('image')->clientExtension()}";
            Storage::putFileAs('/upload/' . $path, $request->file('image'), $fileName);
            $product->update(['image' => '/storage/upload/' . trim($path, '/') . '/' . trim($fileName, '/')]);
        }

        return Redirect::route('products.admin.edit', $id)->with('status', 'product-updated');
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id): void
    {
        Product::query()->findOrFail($id)->update([
            'status' => false
        ]);
    }

    public function recovery(int $id): void
    {
        Product::query()->findOrFail($id)->update([
            'status' => true
        ]);
    }

    public function adminDestroy(int $id): RedirectResponse
    {
        Product::query()->find($id)->update([
            'status' => false
        ]);

        return redirect()->route('products.admin.index');
    }

    public function adminRecovery(int $id): RedirectResponse
    {
        Product::query()->find($id)->update([
            'status' => true
        ]);

        return redirect()->route('products.admin.show', $id);
    }
}
