<?php

namespace App\Http\Modules\Authors;

use App\Http\Controllers\Controller;
use App\Models\Author;
use App\Traits\FilterTrait;
use App\Traits\OrderTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;

class AuthorsController extends Controller
{
    use FilterTrait;
    use OrderTrait;

    public static array $orderFields = [
        'fio',
    ];

    public static array $filterFields = [
        'fio' => [
            'type' => '',
            'action' => 'like'
        ],
        'status' => [
            'type' => '',
            'action' => '='
        ],
    ];

    public function __construct()
    {
        $this->middleware('permission:authors_view', ['only' => ['index']]);
        $this->middleware('permission:authors_edit', ['only' => ['create', 'store', 'edit', 'update', 'destroy']]);
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request): Response
    {
        self::setDefaultOrder(['id' => 'DESC']);
        $authors = Author::query();
        $authors = self::filterData($request, $authors);
        $authors = self::orderData($request, $authors);
        $authors = $authors->paginate(6);

        return response()->view('authors.index', [
            'authors' => $authors,
            'order' => self::orderGenerate($request),
            'filter' => self::filterGenerate($request),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(): Response
    {
        return response()->view('authors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'fio' => ['required', 'string', Rule::unique(Author::class, 'fio')],
        ]);
        $fields = [
            'fio' => $request->post('fio')
        ];
        Author::create($fields);

        return redirect()->route('authors.index')
            ->with('success', __('authors.messages.store'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show(int $id): Response
    {
        $author = Author::query()->find($id);
        $history = $author->getHistory();

        return response()->view('authors.show', [
            'author' => $author,
            'history' => $history,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit(int $id): Response
    {
        $author = Author::find($id);

        return response()->view('authors.edit', [
            'author' => $author,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        $request->validate([
            'fio' => ['required', 'string', Rule::unique(Author::class, 'fio')->ignore($id)],
        ]);

        $fields = [
            'fio' => $request->post('fio'),
        ];
        Author::find($id)->update($fields);

        return redirect()->route('authors.index')
            ->with('success', __('authors.messages.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        Author::find($id)->update([
            'status' => false
        ]);

        return redirect()->route('authors.index')
            ->with('success', __('authors.messages.delete'));
    }

    /**
     * @param int $id
     * @return RedirectResponse
     */
    public function recovery(int $id): RedirectResponse
    {
        Author::find($id)->update([
            'status' => true
        ]);

        return redirect()->route('authors.show', $id)
            ->with('success', __('authors.messages.recovery'));
    }

    public function autocomplete(): JsonResponse
    {
        return response()->json(Author::autocomplete());
    }
}
