<?php

namespace App\Http\Modules\Files;

use App\Http\Controllers\Controller;
use App\Models\Files;
use App\Models\Users\User;
use App\Traits\FilterTrait;
use App\Traits\OrderTrait;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class FilesController extends Controller
{
    use FilterTrait;
    use OrderTrait;

    public static array $orderFields = [
        'title',
        'creator_id',
        'status'
    ];

    public static array $filterFields = [
        'title' => [
            'type' => '',
            'action' => 'like'
        ],
        'status' => [
            'type' => '',
            'action' => '='
        ],
    ];

    public function __construct()
    {
        $this->middleware('permission:files_view', ['only' => ['index', 'show']]);
        $this->middleware('permission:files_edit', ['only' => ['create', 'store', 'edit', 'update', 'destroy']]);
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request): Response
    {
        self::setDefaultOrder(['id' => 'DESC']);
        $files = Files::query();
        $files = self::filterData($request, $files);
        $files = self::orderData($request, $files);
        $files = $files->paginate(6);

        return response()->view('files.index', [
            'files' => $files,
            'order' => self::orderGenerate($request),
            'filter' => self::filterGenerate($request),
            'users_select' => User::autocomplete(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(): Response
    {
        return response()->view('files.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'title' => ['required', 'string', Rule::unique(Files::class, 'title')],
            'file' => ['required', 'file'],
        ]);
        $path = "/files/";
        $fileName = "{$request->post('title')}.{$request->file('file')->clientExtension()}";
        Storage::putFileAs('/upload' . $path, $request->file('file'), $fileName);
        $fields = [
            'title' => $request->post('title'),
            'creator_id' => Auth::id(),
            'path' => '/storage/upload' . $path . $fileName
        ];
        Files::create($fields);

        return redirect()->route('files.index')
            ->with('success', __('files.messages.store'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show(int $id): Response
    {
        $files = Files::query()->find($id);
        $history = $files->getHistory();

        return response()->view('files.show', [
            'file' => $files,
            'history' => $history,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit(int $id): Response
    {
        $files = Files::find($id);

        return response()->view('files.edit', [
            'file' => $files,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        $request->validate([
            'title' => ['required', 'string', Rule::unique(Files::class, 'title')->ignore($id)],
            'file' => ['required', 'file'],
        ]);
        $path = "/files/";
        $fileName = "{$request->post('title')}.{$request->file('file')->clientExtension()}";
        Storage::putFileAs('/upload' . $path, $request->file('file'), $fileName);
        $fields = [
            'title' => $request->post('title'),
            'path' => '/storage/upload' . $path . $fileName
        ];
        Files::find($id)->update($fields);

        return redirect()->route('files.index')
            ->with('success', __('files.messages.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        Files::find($id)->update([
            'status' => false
        ]);

        return redirect()->route('files.index')
            ->with('success', __('files.messages.delete'));
    }

    /**
     * @param int $id
     * @return RedirectResponse
     */
    public function recovery(int $id): RedirectResponse
    {
        Files::find($id)->update([
            'status' => true
        ]);

        return redirect()->route('files.show', $id)
            ->with('success', __('files.messages.recovery'));
    }
}
