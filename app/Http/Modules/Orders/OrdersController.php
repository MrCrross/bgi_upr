<?php

namespace App\Http\Modules\Orders;

use App\Exceptions\CoreException;
use App\Http\Controllers\Controller;
use App\Http\Modules\Basket\BasketController;
use App\Models\Basket;
use App\Models\Order;
use App\Models\OrdersProduct;
use App\Models\OrdersStatus;
use App\Models\Product;
use App\Models\Users\User;
use App\Traits\FilterTrait;
use App\Traits\OrderTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\Rule;
use Illuminate\Support\Carbon;

class OrdersController extends Controller
{
    use FilterTrait;
    use OrderTrait;

    public static array $orderFields = [
        'customer_id',
        'date'
    ];

    public static array $filterFields = [
        'max_date' => [
            'type' => null,
            'action' => '<='
        ],
        'min_date' => [
            'type' => null,
            'action' => '>='
        ],
        'customer_id' => [
            'type' => '0',
            'action' => '='
        ],
    ];

    public function __construct()
    {
        $this->middleware('permission:orders_view|orders_create|orders_edit', ['show']);
        $this->middleware('permission:orders_view', ['only' => ['index']]);
        $this->middleware('permission:orders_create', ['only' => ['create', 'store', 'repeat', 'cancel']]);
        $this->middleware('permission:orders_edit', ['only' => ['edit', 'update', 'destroy']]);
    }

    public function datatable(Request $request, bool $currentUser = false): array
    {
        self::setDefaultOrder(['date' => 'desc']);
        $orders = Order::with(
            'status',
            'customer',
            'products.product'
        );
        if (Auth::user()->can('orders_view-delete')) {
            $orders->withTrashed();
        }
        if ($currentUser) {
            $orders = $orders->where('customer_id', '=', Auth::id());
        }
        $orders = self::filterData($request, $orders);
        $orders = self::orderData($request, $orders);
        $orders = $orders->paginate(15);

        return [
            'orders' => $orders,
            'users_select' => User::autocomplete(),
            'orders_filter' => self::filterGenerate($request),
            'orders_order' => self::orderGenerate($request)
        ];
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): Response
    {
        return response()->view('orders.index', $this->datatable($request));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request): Response
    {
        return response()->view('orders.create', [
            'products_select' => Product::autocomplete(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'products' => ['required', 'array'],
            'products.*' => ['required', 'array'],
            'products.*.id' => ['required', 'integer', Rule::exists(Product::class, 'id')],
            'products.*.quantity' => ['required', 'integer', 'min:1'],
        ]);
        $fields = [
            'status_id' => 1,
            'customer_id' => Auth::user()->id,
            'date' => Carbon::now()->toDateTime(),
        ];

        $orderID = Order::query()->create($fields)->id;

        foreach ($request->post('products') as $product) {
            $orderProductFields = [
                'order_id' => $orderID,
                'product_id' => $product['id'],
                'quantity' => $product['quantity'],
            ];
            OrdersProduct::query()->create($orderProductFields);
        }

        return Redirect::route('orders.create')->with('status', 'order-created');
    }

    /**
     * Display the specified resource.
     * @throws CoreException
     */
    public function show(int $id): Response
    {
        $order = Order::with(
            'status',
            'customer',
            'products.product'
        )
            ->withTrashed()
            ->find($id);
        if (empty($order)) {
            throw new CoreException('no_object');
        }
        $history = $order->getHistory();

        return response()->view('orders.show', [
            'order' => $order,
            'history' => $history,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(int $id): Response
    {
        $order = Order::with(
            'status',
            'customer',
            'products.product'
        )
            ->withTrashed()
            ->find($id);
        $statuses = OrdersStatus::select('id as value', 'name as label')
            ->get();

        return response()->view('orders.edit', [
            'order' => $order,
            'statuses' => $statuses,
            'products' => Product::autocomplete(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id): RedirectResponse
    {
        $request->validate([
            'status_id' => ['required', 'integer', Rule::exists(OrdersStatus::class, 'id')],
            'products' => ['required', 'array'],
            'products.*' => ['required', 'array'],
            'products.*.id' => ['required', 'integer', Rule::exists(Product::class, 'id')],
            'products.*.quantity' => ['required', 'integer', 'min:1'],
        ]);

        $fields = [
            'status_id' => $request->post('status_id'),
        ];

        Order::withTrashed()->find($id)->update($fields);

        if ($request->has('products')) {
            OrdersProduct::query()
                ->where('order_id', '=', $id)
                ->delete();
            foreach ($request->post('products') as $product) {
                $orderProductFields = [
                    'order_id' => $id,
                    'product_id' => $product['id'],
                    'quantity' => $product['quantity'],
                ];
                OrdersProduct::query()->create($orderProductFields);
            }
        }

        return Redirect::route('orders.edit', $id)->with('status', 'order-updated');
    }

    public function repeat(int $id): RedirectResponse
    {
        $order = Order::with(
            'status',
            'customer',
            'products.product'
        )
            ->withTrashed()
            ->find($id);

        $repeatID = Order::query()->create([
            'status_id' => 1,
            'customer_id' => Auth::user()->id,
            'date' => Carbon::now()->toDateTime(),
        ])->id;

        if (!empty($order->products) && $order->products->isNotEmpty()) {
            foreach ($order->products as $product) {
                OrdersProduct::query()->create([
                    'order_id' => $repeatID,
                    'product_id' => $product->product_id,
                    'quantity' => $product->quantity,
                ]);
            }
        }

        return Redirect::route('orders.show', $repeatID)->with('status', 'order-repeat');
    }

    public function cancel(int $id): RedirectResponse
    {
        Order::query()->find($id)->update([
            'status_id' => 7
        ]);

        return Redirect::route('orders.show', $id)->with('status', 'order-cancel');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id): RedirectResponse
    {
        Order::query()->find($id)->delete();

        return redirect()->route('orders.index');
    }

    public function recovery(int $id): RedirectResponse
    {
        $order = Order::withTrashed()->find($id);
        $order->restore();
        $order->update([
            'status_id' => 1,
        ]);

        return redirect()->route('orders.index');
    }

    public function storeByBasket(): JsonResponse
    {
        $basket = Basket::query()->where('owner_id', '=', Auth::id())->get();

        $repeatID = Order::query()->create([
            'status_id' => 1,
            'customer_id' => Auth::id(),
            'date' => Carbon::now()->toDateTime(),
        ])->id;

        if (!empty($basket) && $basket->isNotEmpty()) {
            foreach ($basket as $row) {
                OrdersProduct::query()->create([
                    'order_id' => $repeatID,
                    'product_id' => $row->product_id,
                    'quantity' => $row->count,
                ]);
            }
        }

        (new BasketController())->destroy();

        return response()->json([]);
    }
}
