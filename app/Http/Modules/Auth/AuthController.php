<?php

namespace App\Http\Modules\Auth;

use App\Exceptions\CoreException;
use App\Http\Controllers\Controller;
use App\Http\Modules\Auth\Exceptions\AuthLoginException;
use App\Http\Modules\Auth\Requests\AuthLoginRequest;
use App\Http\Modules\Auth\Requests\AuthRegistrationRequest;
use App\Http\Modules\Auth\Services\AuthLoginService;
use App\Http\Modules\Auth\Services\AuthRegistrationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * @throws AuthLoginException
     * @throws CoreException
     */
    public function login(
        AuthLoginRequest $request,
        AuthLoginService $loginService
    ): JsonResponse {
        return response()->json($loginService->login($request));
    }

    public function getCurrentUser(): JsonResponse
    {
        $user = Auth::user();
        $permissions = $user->getPermissionsViaRoles()->map(function ($item) {
            return [
                'id' => $item->id,
                'name' => $item->name,
            ];
        });
        $user->permissions = $permissions;

        return response()->json($user);
    }

    /**
     * @param AuthRegistrationRequest $request
     * @param AuthRegistrationService $registrationService
     * @return JsonResponse
     */
    public function registration(
        AuthRegistrationRequest $request,
        AuthRegistrationService $registrationService
    ): JsonResponse {
        return response()->json($registrationService->registration($request));
    }

    public function logout(
        AuthLoginService $loginService
    ): JsonResponse {
        return response()->json($loginService->logout());
    }
}
