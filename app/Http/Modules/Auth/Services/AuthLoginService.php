<?php

namespace App\Http\Modules\Auth\Services;

use App\Exceptions\CoreException;
use App\Http\Middleware\JWTValidation;
use App\Models\Users\User;
use App\Http\Modules\Auth\Exceptions\AuthLoginException;
use App\Http\Modules\Auth\Requests\AuthLoginRequest;
use App\Models\Users\UsersAuthorization;
use App\Models\Users\UsersAuthorizationLog;
use hisorange\BrowserDetect\Parser as Browser;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request;
use ReallySimpleJWT\Token;

class AuthLoginService
{
    private static int $amountBeforeBan = 5;
    private static int $minutesBan = 20;

    /**
     * @param AuthLoginRequest $request
     * @return array
     * @throws CoreException
     * @throws AuthLoginException
     */
    public function login(AuthLoginRequest $request): array
    {
        $existLog = UsersAuthorizationLog::query()
            ->select('id', 'count', 'date_login')
            ->where('ip', Request::ip())
            ->first();
        $middle = Carbon::now()->subMinutes(self::$minutesBan);
        if (
            !empty($existLog) &&
            $existLog['count'] >= self::$amountBeforeBan &&
            $middle <= $existLog['date_login']
        ) {
            throw new AuthLoginException('ban');
        }
        $user = User::query()
            ->where('email', '=', $request->post('email'))
            ->first();
        if (empty($user) || !Hash::check($request->post('password'), $user->password)) {
            $this->authLogs($request->post('email'));
            throw new AuthLoginException('invalid');
        }
        if ($user->status === 0) {
            throw new CoreException('user_off');
        }
        if (!empty($existLog)) {
            UsersAuthorizationLog::query()
                ->where('ip', '=', Request::ip())
                ->delete();
        }
        Auth::loginUsingId($user->id);
        Auth::getSession()->regenerate();

        return self::createToken($user->id);
    }

    /**
     * @param string $email
     * @return void
     */
    private function authLogs(string $email): void
    {
        $browser = $this->getBrowser();
        $existLog = UsersAuthorizationLog::query()
            ->select('id', 'count', 'date_login')
            ->where('ip', Request::ip())
            ->first();
        if (empty($existLog)) {
            UsersAuthorizationLog::query()
                ->create([
                    'ip' => Request::ip(),
                    'email' => $email,
                    'count' => 1,
                    'browser' => $browser,
                    'date_login' => Carbon::now()
                ]);
        } else {
            $middle = Carbon::now()->subMinutes(self::$minutesBan);
            if (
                $existLog['count'] <= self::$amountBeforeBan &&
                $middle <= $existLog['date_login']
            ) {
                UsersAuthorizationLog::query()
                    ->where('id', $existLog['id'])
                    ->update([
                        'ip' => Request::ip(),
                        'email' => $email,
                        'count' => $count ?? DB::raw('count + 1'),
                        'browser' => $browser,
                        'date_login' => Carbon::now()
                    ]);
            } elseif (
                $existLog['count'] >= self::$amountBeforeBan &&
                $middle >= $existLog['date_login']
            ) {
                UsersAuthorizationLog::query()
                    ->where('id', $existLog['id'])
                    ->update([
                        'ip' => Request::ip(),
                        'email' => $email,
                        'count' => 1,
                        'browser' => $browser,
                        'date_login' => Carbon::now()
                    ]);
            }
        }
    }

    private static function getBrowser(): string
    {
        $browser = new Browser();
        $browserInfo = $browser->detect();

        return $browserInfo->isBot() ?
            $browserInfo->userAgent() :
            $browserInfo->browserName() . ';' . $browserInfo->platformName();
    }

    /**
     * @param int $userID
     * @param bool $isRefresh
     * @param string $oldToken
     * @return array
     */
    public static function createToken(int $userID, bool $isRefresh = false, string $oldToken = ''): array
    {
        $browser = self::getBrowser();
        $expired = Carbon::now()->addDays(7);
        $payload = [
            'user_id' => $userID,
            'expire' => $expired,
        ];
        $token = Token::customPayload($payload, env('SECRET_JWT'));
        if ($isRefresh) {
            UsersAuthorization::query()
                ->where([
                    ['user_id', '=', $userID],
                    ['token', '=', $oldToken]
                ])
                ->update([
                    'token' => $token,
                    'expired' => $expired,
                    'browser' => $browser,
                    'ip' => Request::ip()
                ]);
        } else {
            UsersAuthorization::query()->insert([
                'user_id' => $userID,
                'token' => $token,
                'expired' => $expired,
                'browser' => $browser,
                'ip' => Request::ip()
            ]);
        }

        Cookie::queue(
            Cookie::make(
                'token',
                $token,
                7 * 24 * 60,
                '/',
                Request::getHost(),
                Request::secure(),
                false
            )
        );

        return [
            'token' => $token
        ];
    }

    /**
     * @return array
     */
    public function refreshToken(): array
    {
        return self::createToken(Auth::id(), true, JWTValidation::$token);
    }

    public function logout(): array
    {
        UsersAuthorization::query()
            ->where([
                ['user_id', '=', Auth::id()],
                ['token', '=', JWTValidation::$token]
            ])
            ->update([
                'expired' => now()->subMinute(),
            ]);
        Auth::logout();

        return [
            'message' => 'success'
        ];
    }
}
