<?php

namespace App\Http\Modules\Auth\Services;

use App\Models\Users\User;
use App\Http\Modules\Auth\Requests\AuthRegistrationRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class AuthRegistrationService
{
    /**
     * @param AuthRegistrationRequest $request
     *
     * @return array
     */
    public function registration(AuthRegistrationRequest $request): array {
        $defaultRole = Role::query()
            ->select('id')
            ->where('name', '=', 'default')
            ->value('id');

        $fields = [
            'email' => $request->post('email'),
            'password' => Hash::make($request->post('password')),
            'name' => $request->post('name'),
            'is_active' => true
        ];

        $user = User::query()->create($fields);
        $user->syncRoles($defaultRole);

        Auth::loginUsingId($user->id);
        Auth::getSession()->regenerate();

        return AuthLoginService::createToken($user->id);
    }
}
