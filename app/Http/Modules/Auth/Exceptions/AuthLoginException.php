<?php

namespace App\Http\Modules\Auth\Exceptions;

use App\Wrappers\ExceptionWrapper;

class AuthLoginException extends ExceptionWrapper
{
    /**
     * @return array
     */
    protected function setErrors(): array
    {
        return [
            'invalid' => 'Не верная почта или пароль',
            'ban' => 'Вы заблокированы за превышение количества попыток авторизации',
        ];
    }
}
