<?php

namespace App\Http\Modules\Auth\Requests;

use App\Exceptions\CoreException;
use App\Models\Users\User;
use App\Traits\FormRequestValidationTrait;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AuthRegistrationRequest extends FormRequest
{
    use FormRequestValidationTrait;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique(User::class, 'email')],
            'password' => 'required|string|min:8|max:255|confirmed',
            'name' => 'required|string|max:255',
        ];
    }


}
