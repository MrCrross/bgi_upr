<?php

namespace App\Http\Modules\Basket;

use App\Http\Controllers\Controller;
use App\Models\Basket;
use App\Models\Product;
use App\Models\Users\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class BasketController extends Controller
{
    public function index(): JsonResponse
    {
        $user = User::query()->with('basket.product')->find(Auth::id());

        return response()->json([
            'basket' => $user->basket
        ]);
    }

    public function store(int $productID): void
    {
        $ownerID = Auth::user()->id;
        $count = (int)Basket::query()
            ->where('owner_id', '=', $ownerID)
            ->where('product_id', '=', $productID)
            ->value('count');
        if ($count === 0) {
            Basket::query()
                ->create([
                    'owner_id' => $ownerID,
                    'product_id' => $productID,
                    'count' => 1
                ]);
        } else {
            Basket::query()
                ->where('owner_id', '=', $ownerID)
                ->where('product_id', '=', $productID)
                ->update([
                    'count' => $count + 1
                ]);
        }
    }

    public function update(Request $request): JsonResponse
    {
        if (!$request->has('products')) {
            Basket::query()
                ->where('owner_id', '=', Auth::id())
                ->delete();
        } else {
            $request->validate([
                'products' => ['required', 'array'],
                'products.*' => ['required', 'array'],
                'products.*.id' => ['required', 'integer', Rule::exists(Product::class, 'id')],
                'products.*.count' => ['required', 'integer', 'min:1'],
            ]);
            $ownerID = Auth::id();
            foreach ($request->post('products') as $product) {
                $basketID = Basket::query()
                    ->where('owner_id', '=', $ownerID)
                    ->where('product_id', '=', $product['id'])
                    ->value('id');
                Basket::query()
                    ->where('id', '=', $basketID)
                    ->update([
                        'count' => $product['count']
                    ]);
            }
        }

        $user = User::query()->with('basket.product')->find(Auth::id());

        return response()->json([
            'basket' => $user->basket
        ]);
    }

    public function destroy(): void
    {
        Basket::query()
            ->where('owner_id', Auth::id())
            ->delete();
    }
}
