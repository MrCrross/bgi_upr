<?php

namespace App\Http\Modules\Users\Requests;

use App\Models\Users\User;
use App\Traits\FormRequestValidationTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;

class UsersStoreRequest extends FormRequest
{
    use FormRequestValidationTrait;

    public function authorize(): true
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'email' => ['required', 'email', Rule::unique(User::class, 'email')],
            'password' => ['required', 'confirmed', Password::defaults()],
            'roles' => ['required', 'array'],
        ];
    }
}
