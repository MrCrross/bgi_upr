<?php

namespace App\Http\Modules\Publishers;

use App\Exceptions\CoreException;
use App\Http\Controllers\Controller;
use App\Models\Publisher;
use App\Traits\FilterTrait;
use App\Traits\OrderTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;

class PublishersController extends Controller
{
    use FilterTrait;
    use OrderTrait;

    public static array $orderFields = [
        'name',
    ];

    public static array $filterFields = [
        'name' => [
            'type' => '',
            'action' => 'like'
        ],
        'status' => [
            'type' => '',
            'action' => '='
        ],
    ];

    public function __construct()
    {
        $this->middleware('permission:publishers_view', ['only' => ['index']]);
        $this->middleware('permission:publishers_edit', ['only' => ['create', 'store', 'edit', 'update', 'destroy']]);
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request): Response
    {
        self::setDefaultOrder(['id' => 'DESC']);
        $publishers = Publisher::query();
        $publishers = self::filterData($request, $publishers);
        $publishers = self::orderData($request, $publishers);
        $publishers = $publishers->paginate(6);

        return response()->view('publishers.index', [
            'publishers' => $publishers,
            'order' => self::orderGenerate($request),
            'filter' => self::filterGenerate($request),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(): Response
    {
        return response()->view('publishers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'name' => ['required', 'string', Rule::unique(Publisher::class, 'name')],
        ]);
        $fields = [
            'name' => $request->post('name')
        ];
        Publisher::create($fields);

        return redirect()->route('publishers.index')
            ->with('success', __('publishers.messages.store'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     * @throws CoreException
     */
    public function show(int $id): Response
    {
        $publisher = Publisher::query()->find($id);
        if (empty($publisher)) {
            throw new CoreException('no_object');
        }
        $history = $publisher->getHistory();

        return response()->view('publishers.show', [
            'publisher' => $publisher,
            'history' => $history,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit(int $id): Response
    {
        $publisher = Publisher::find($id);

        return response()->view('publishers.edit', [
            'publisher' => $publisher,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        $request->validate([
            'name' => ['required', 'string', Rule::unique(Publisher::class, 'name')->ignore($id)],
        ]);

        $fields = [
            'name' => $request->post('name'),
        ];
        Publisher::find($id)->update($fields);

        return redirect()->route('publishers.index')
            ->with('success', __('publishers.messages.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        Publisher::find($id)->update([
            'status' => false
        ]);

        return redirect()->route('publishers.index')
            ->with('success', __('publishers.messages.delete'));
    }

    /**
     * @param int $id
     * @return RedirectResponse
     */
    public function recovery(int $id): RedirectResponse
    {
        Publisher::find($id)->update([
            'status' => true
        ]);

        return redirect()->route('publishers.show', $id)
            ->with('success', __('publishers.messages.recovery'));
    }

    public function autocomplete(): JsonResponse
    {
        return response()->json(Publisher::autocomplete());
    }
}
