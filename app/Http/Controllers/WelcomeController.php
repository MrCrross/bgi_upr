<?php

namespace App\Http\Controllers;

use App\Models\News;
use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function index(Request $request): JsonResponse
    {
        $products = Product::query()
            ->with(['publisher', 'authors', 'genres'])
            ->where('status', '=', true)
            ->orderBy('id', 'desc')
            ->limit(5)
            ->get();
        $news = News::query()
            ->with('creator')
            ->where('status', '=', true)
            ->orderBy('id', 'desc')
            ->limit(5)
            ->get();

        return response()->json(compact('products', 'news'));
    }
}
