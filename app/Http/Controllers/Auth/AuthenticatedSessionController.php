<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Middleware\JWTValidation;
use App\Http\Modules\Auth\Services\AuthLoginService;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\Users\UsersAuthorization;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     */
    public function create(): View
    {
        return view('auth.login');
    }

    /**
     * Handle an incoming authentication request.
     */
    public function store(LoginRequest $request): RedirectResponse
    {
        $request->authenticate();

        $request->session()->regenerate();
        AuthLoginService::createToken(Auth::id());

        return redirect()->intended(route('profile.edit', Auth::id(), absolute: false));
    }

    /**
     * Destroy an authenticated session.
     */
    public function destroy(Request $request): RedirectResponse
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();
        UsersAuthorization::query()
            ->where([
                ['user_id', '=', Auth::id()],
                ['token', '=', JWTValidation::$token]
            ])
            ->update([
                'expired' => now()->subMinute(),
            ]);

        return redirect('/');
    }
}
