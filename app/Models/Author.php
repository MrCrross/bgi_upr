<?php

namespace App\Models;

use App\Traits\HistoryModelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Panoscape\History\HasHistories;

class Author extends Model
{
    use HasFactory, HasHistories, HistoryModelTrait;
    protected $table = 'authors';
    protected $guarded = [];
    public $timestamps = true;

    public static function autocomplete(): Collection
    {
        return self::query()
            ->select('id as value', 'fio as label')
            ->where('status', '=', true)
            ->orderBy('fio')
            ->get();
    }

    public function getModelLabel()
    {
        return $this->fio;
    }

    public function historyMetaFields(): array
    {
        return [
            'fio' => [
                'name' => 'ФИО',
            ],
            'status' => [
                'name' => 'Статус',
            ],
        ];
    }
}
