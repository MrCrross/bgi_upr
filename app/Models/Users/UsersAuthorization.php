<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UsersAuthorization extends Model
{
    use HasFactory;
    protected $table = 'users_authorization';
    public $timestamps = false;
    protected $guarded = [];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public static function activeSession(int $userID, string $token)
    {
        return self::select('*')->where('user_id', '=', $userID)
            ->where('token', '=', $token)
            ->first();
    }
}
