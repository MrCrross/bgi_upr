<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsersAuthorizationLog extends Model
{
    use HasFactory;

    protected $table = 'users_authorization_logs';
    protected $guarded = [];
    public $timestamps = false;
}
