<?php

namespace App\Models\Users;

use App\Models\Basket;
use App\Traits\HistoryModelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Panoscape\History\HasHistories;
use Panoscape\History\HasOperations;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasOperations, HasHistories, HasRoles, HistoryModelTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'is_active',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Get the attributes that should be cast.
     *
     * @return array<string, string>
     */
    protected function casts(): array
    {
        return [
            'email_verified_at' => 'datetime',
            'password' => 'hashed',
        ];
    }

    public function basket(): HasMany
    {
        return $this->hasMany(Basket::class, 'owner_id', 'id');
    }


    public static function autocomplete(): Collection
    {
        $result = collect([
            (object)[
                'value' => '',
                'label' => __('datatable.no_selected'),
                'status' => 1
            ]
        ]);

        return $result->merge(
            self::query()
                ->select('id as value', 'name as label', 'is_active')
                ->where('is_active', '=', 1)
                ->orderBy('name')
                ->get()
        );
    }

    public static function frontAutocomplete(): Collection
    {
        return self::query()
            ->select('id as value', 'name as label', 'is_active')
            ->where('is_active', '=', 1)
            ->orderBy('name')
            ->get();
    }

    public function getModelLabel()
    {
        return $this->name;
    }

    public function historyMetaFields(): array
    {
        return [
            'name' => [
                'name' => 'ФИО',
            ],
            'email' => [
                'name' => 'Адрес электронной почты',
            ],
            'email_verified_at' => [
                'name' => 'Верификация почты',
            ],
            'password' => [
                'name' => 'Пароль'
            ],
            'is_active' => [
                'name' => 'Активный'
            ],
        ];
    }
}
