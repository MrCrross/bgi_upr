<?php

namespace App\Models;

use App\Models\Users\User;
use App\Traits\HistoryModelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Panoscape\History\HasHistories;

class News extends Model
{
    use HasFactory, HasHistories, HistoryModelTrait;

    protected $table = 'news';
    protected $guarded = [];
    public $timestamps = true;

    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    public function getModelLabel()
    {
        return $this->title;
    }

    public function historyMetaFields(): array
    {
        return [
            'date' => [
                'name' => 'Дата издания',
            ],
            'title' => [
                'name' => 'Название',
            ],
            'content' => [
                'name' => 'Содержание',
            ],
            'image' => [
                'name' => 'Изображение',
            ],
            'status' => [
                'name' => 'Статус',
            ],
        ];
    }
}
