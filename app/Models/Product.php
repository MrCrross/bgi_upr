<?php

namespace App\Models;

use App\Traits\HistoryModelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Collection;
use Panoscape\History\HasHistories;

class Product extends Model
{
    use HasFactory, HasHistories, HistoryModelTrait;

    protected $table = 'products';
    protected $guarded = [];
    public $timestamps = true;

    public function authors(): BelongsToMany
    {
        return $this->belongsToMany(Author::class, 'products_authors', 'product_id', 'author_id');
    }

    public function genres(): BelongsToMany
    {
        return $this->belongsToMany(Genre::class, 'products_genres', 'product_id', 'genre_id');
    }

    public function publisher(): BelongsTo
    {
        return $this->belongsTo(Publisher::class, 'publisher_id', 'id');
    }

    public function getModelLabel()
    {
        return $this->name;
    }

    public static function autocomplete(): Collection
    {
        $result = collect([
            (object)[
                'value' => '',
                'label' => __('datatable.no_selected'),
                'price' => 0,
            ]
        ]);

        return $result->merge(
            self::query()
                ->select('id as value', 'name as label', 'price')
                ->where('status', 1)
                ->orderBy('name')
                ->get()
        );
    }

    public function historyMetaFields(): array
    {
        return [
            'isbn' => [
                'name' => 'ISBN',
            ],
            'price' => [
                'name' => 'Стоимость',
            ],
            'description' => [
                'name' => 'Описание',
            ],
            'image' => [
                'name' => 'Изображение'
            ],
            'name' => [
                'name' => 'Название',
            ],
            'pages' => [
                'name' => 'Страницы',
            ],
            'year_published' => [
                'name' => 'Год издания',
            ],
            'status' => [
                'name' => 'Статус',
            ],
            'publisher_id' => [
                'name' => 'Издатель',
                'table' => [
                    'name' => 'publishers',
                    'value' => 'id',
                    'label' => 'name',
                ],
            ],
        ];
    }
}
