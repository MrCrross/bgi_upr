<?php

namespace App\Models;

use App\Traits\HistoryModelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Panoscape\History\HasHistories;

class Files extends Model
{
    use HasFactory, HasHistories, HistoryModelTrait;

    protected $table = 'files';
    protected $guarded = [];
    public $timestamps = true;

    public function getModelLabel()
    {
        return $this->title;
    }

    public function historyMetaFields(): array
    {
        return [
            'title' => [
                'name' => 'Название',
            ],
            'creator_id' => [
                'name' => 'Добавил',
                'table' => [
                    'name' => 'users',
                    'value' => 'id',
                    'label' => 'name',
                ],
            ],
            'status' => [
                'name' => 'Статус',
            ],
        ];
    }
}
