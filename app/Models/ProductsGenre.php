<?php

namespace App\Models;

use App\Traits\HistoryModelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Panoscape\History\HasHistories;

class ProductsGenre extends Model
{
    use HasFactory, HasHistories, HistoryModelTrait;
    protected $table = 'products_genres';
    protected $guarded = [];
    public $timestamps = false;

    public function genre(): BelongsTo
    {
        return $this->belongsTo(Genre::class, 'genre_id', 'id');
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function getModelLabel()
    {
        return $this->product->name . ': ' . $this->genre->name;
    }

    public function historyMetaFields(): array
    {
        return [
            'product_id' => [
                'name' => 'Товар',
                'table' => [
                    'name' => 'products',
                    'value' => 'id',
                    'label' => 'name'
                ]
            ],
            'genre_id' => [
                'name' => 'Жанр',
                'table' => [
                    'name' => 'genres',
                    'value' => 'id',
                    'label' => 'name'
                ]
            ],
        ];
    }
}
