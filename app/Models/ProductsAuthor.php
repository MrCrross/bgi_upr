<?php

namespace App\Models;

use App\Traits\HistoryModelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Panoscape\History\HasHistories;

class ProductsAuthor extends Model
{
    use HasFactory, HasHistories, HistoryModelTrait;

    protected $table = 'products_authors';
    protected $guarded = [];
    public $timestamps = false;

    public function author(): BelongsTo
    {
        return $this->belongsTo(Author::class, 'author_id', 'id');
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function getModelLabel()
    {
        return $this->product->name . ': ' . $this->author->fio;
    }

    public function historyMetaFields(): array
    {
        return [
            'product_id' => [
                'name' => 'Товар',
                'table' => [
                    'name' => 'products',
                    'value' => 'id',
                    'label' => 'name'
                ]
            ],
            'author_id' => [
                'name' => 'Автор',
                'table' => [
                    'name' => 'authors',
                    'value' => 'id',
                    'label' => 'fio'
                ]
            ],
        ];
    }
}
