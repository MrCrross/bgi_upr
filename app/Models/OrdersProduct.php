<?php

namespace App\Models;

use App\Traits\HistoryModelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use Panoscape\History\HasHistories;

class OrdersProduct extends Model
{
    use HasFactory, HasHistories, HistoryModelTrait;

    protected $table = 'orders_products';
    protected $guarded = [];
    public $timestamps = false;

    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function getModelLabel()
    {
        return '№' . $this->order->id . ' от ' . Carbon::parse($this->order->date)->format('d.m.Y H:i');
    }

    public function historyMetaFields(): array
    {
        return [
            'product_id' => [
                'name' => 'Товар',
                'table' => [
                    'name' => 'products',
                    'value' => 'id',
                    'label' => 'name',
                ]
            ],
            'quantity' => [
                'name' => 'Количество',
            ],
        ];
    }
}
