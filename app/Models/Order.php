<?php

namespace App\Models;

use App\Models\Users\User;
use App\Traits\HistoryModelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Panoscape\History\HasHistories;

class Order extends Model
{
    use HasFactory, HasHistories, HistoryModelTrait, SoftDeletes;

    protected $table = 'orders';
    protected $guarded = [];
    public $timestamps = true;

    public function products(): HasMany
    {
        return $this->hasMany(OrdersProduct::class, 'order_id', 'id');
    }

    public function customer(): BelongsTo
    {
        return $this->belongsTo(User::class, 'customer_id', 'id');
    }

    public function status(): BelongsTo
    {
        return $this->belongsTo(OrdersStatus::class, 'status_id', 'id');
    }

    public function getModelLabel()
    {
        return '№' . $this->id . ' от ' . Carbon::parse($this->date)->format('d.m.Y H:i');
    }

    public function historyMetaFields(): array
    {
        return [
            'date' => [
                'name' => 'Дата заказа',
            ],
            'customer_id' => [
                'name' => 'Заказчик',
                'table' => [
                    'name' => 'users',
                    'value' => 'id',
                    'label' => 'name',
                ],
            ],
            'status_id' => [
                'name' => 'Статус',
                'table' => [
                    'name' => 'orders_statuses',
                    'value' => 'id',
                    'label' => 'name',
                ]
            ],
        ];
    }
}
