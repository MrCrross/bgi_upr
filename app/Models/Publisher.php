<?php

namespace App\Models;

use App\Traits\HistoryModelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Panoscape\History\HasHistories;

class Publisher extends Model
{
    use HasFactory, HasHistories, HistoryModelTrait;
    protected $table = 'publishers';
    protected $guarded = [];
    public $timestamps = true;

    public static function autocomplete(): Collection
    {
        return self::query()
            ->select('id as value', 'name as label')
            ->where('status', '=', true)
            ->orderBy('name')
            ->get();
    }

    public static function select(): Collection
    {
        $result = collect([
            (object)[
                'value' => '',
                'label' => __('datatable.no_selected'),
            ]
        ]);

        return $result->merge(
            self::query()
                ->select('id as value', 'name as label')
                ->orderBy('name')
                ->get()
        );
    }

    public function getModelLabel()
    {
        return $this->name;
    }

    public function historyMetaFields(): array
    {
        return [
            'name' => [
                'name' => 'Название',
            ],
            'status' => [
                'name' => 'Статус',
            ],
        ];
    }
}
