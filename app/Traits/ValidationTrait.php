<?php

namespace App\Traits;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

trait ValidationTrait
{
    private array $tableFields = [];

    /**
     * @param string $table
     * @param array $fields
     * @return array
     */
    protected function validateFields(string $table, array $fields = []): array
    {
        $result = [];
        $this->getTableFields($table);

        foreach ($fields as $field => $value) {
            if (isset($this->tableFields[$table][$field])) {
                if (
                    $this->tableFields[$table][$field]['type'] === 'integer' ||
                    $this->tableFields[$table][$field]['type'] === 'bool'
                ) {
                    $value = (int)$value;
                } elseif (
                    $this->tableFields[$table][$field]['type'] === 'float' ||
                    $this->tableFields[$table][$field] === 'numeric'
                ) {
                    $value = (float)$value;
                }
                if (empty($value) && $this->tableFields[$table][$field]['null']) {
                    $value = null;
                }
                $result[$field] = $value;
            }
        }

        return $result;
    }

    /**
     * @param string $table
     * @return void
     */
    private function getTableFields(string $table): void
    {
        $tableColumns = Schema::getColumns($table);
        foreach ($tableColumns as $column) {
            $type = $column['type_name'];
            if (Str::contains($column['type'], 'int')) {
                $type = 'integer';
            }
            if (Str::contains($column['type'], 'float')) {
                $type = 'float';
            }
            $this->tableFields[$table][$column['name']]['type'] = $type;
            $this->tableFields[$table][$column['name']]['null'] = $column['nullable'];
        }
    }
}
