<?php

namespace App\Traits;

use App\Exceptions\CoreException;
use Illuminate\Contracts\Validation\Validator;

trait FormRequestValidationTrait
{
    /**
     * @param Validator $validator
     * @throws CoreException
     */
    protected function failedValidation(Validator $validator): void
    {
        $errors = $validator->errors();
        $errorMessages = '';
        foreach ($errors->messages() as $field => $messages) {
            foreach ($messages as $message) {
                $errorMessages .= $message . "\n";
            }
        }
        if ($errorMessages !== '') {
            throw new CoreException($errorMessages);
        }

        throw new CoreException('missed_data');
    }
}
