<?php

namespace App\Traits;

trait ResponseTrait
{
    /**
     * @param string|array $message
     * @param array $params
     * @param bool $error
     * @return array
     */
    protected static function sendResponse(string|array $message, array $params = [], bool $error = true): array
    {
        if (is_array($message)) {
            $message = implode('<br>', $message);
        }

        $responseJson = [
            'error' => $error,
            'message' => $message
        ];

        if (!empty($params)) {
            foreach ($params as $key => $value) {
                $responseJson[$key] = $value;
            }
        }

        return $responseJson;
    }
}
