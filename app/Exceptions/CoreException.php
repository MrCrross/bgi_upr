<?php

namespace App\Exceptions;

use App\Wrappers\ExceptionWrapper;

class CoreException extends ExceptionWrapper
{
    protected function setErrors(): array
    {
        return [
            'user_off' => 'Пользователь отключен',
            'missed_data' => 'Недостаточно данных',
            'no_object' => 'Объект отсутствует',
            'no_auth' => 'Не авторизован'
        ];
    }
}
