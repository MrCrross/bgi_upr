<?php


use App\Models\Order;
use App\Models\Product;
use App\Models\Users\User;
use Tests\TestCase;

class OrdersTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    public function test_orders_controller_index_success(): void
    {
        $user = User::factory()->create();
        $user->syncPermissions(['orders_view']);

        $response = $this
            ->actingAs($user)
            ->get('/admin/orders');

        $response->assertViewIs('orders.index');
        $response->assertViewHasAll([
            'orders',
            'users_select',
            'orders_filter',
            'orders_order',
        ]);
        $response->assertStatus(200);
    }

    public function test_orders_controller_index_no_rules(): void
    {
        $user = User::factory()->create();

        $response = $this
            ->actingAs($user)
            ->get('/admin/orders');

        $response->assertStatus(403);
    }

    public function test_orders_controller_show_success(): void
    {
        $order = Order::factory()->create();
        $user = User::factory()->create();
        $user->syncPermissions(['orders_create']);

        $response = $this
            ->actingAs($user)
            ->get('/admin/orders/' . $order->id);

        $response->assertViewIs('orders.show');
        $response->assertViewHasAll([
            'order' => $order,
            'history' => $order->getHistory()
        ]);
        $response->assertStatus(200);
    }

    public function test_orders_controller_show_no_rules(): void
    {
        $order = Order::factory()->create();
        $user = User::factory()->create();

        $response = $this
            ->actingAs($user)
            ->get('/admin/orders/' . $order->id);

        $response->assertStatus(403);
    }

    public function test_orders_controller_create_success(): void
    {
        $user = User::factory()->create();
        $user->syncPermissions(['orders_create']);

        $response = $this
            ->actingAs($user)
            ->get('/admin/orders/create');

        $response->assertViewIs('orders.create');
        $response->assertViewHasAll([
            'products_select',
        ]);
        $response->assertStatus(200);
    }

    public function test_orders_controller_create_no_rules(): void
    {
        $user = User::factory()->create();

        $response = $this
            ->actingAs($user)
            ->get('/admin/orders/create');

        $response->assertStatus(403);
    }

    public function test_orders_controller_edit_success(): void
    {
        $order = Order::factory()->create();
        $user = User::factory()->create();
        $user->syncPermissions(['orders_edit']);

        $response = $this
            ->actingAs($user)
            ->get('/admin/orders/' . $order->id . '/edit');

        $response->assertViewIs('orders.edit');
        $response->assertViewHasAll([
            'order',
            'statuses',
            'products',
        ]);
        $response->assertStatus(200);
    }

    public function test_orders_controller_edit_no_rules(): void
    {
        $order = Order::factory()->create();
        $user = User::factory()->create();

        $response = $this
            ->actingAs($user)
            ->get('/admin/orders/' . $order->id . '/edit');

        $response->assertStatus(403);
    }

    public function test_orders_controller_store_success(): void
    {
        $user = User::factory()->create();
        $product = Product::factory()->create();
        $user->syncPermissions(['orders_create']);

        $response = $this
            ->actingAs($user)
            ->from('/admin/orders/create')
            ->post('/admin/orders', [
                'products' => [
                    [
                        'id' => $product->id,
                        'quantity' => 2,
                    ]
                ],
            ]);

        $response->assertRedirectToRoute('orders.create');
        $response->assertSessionHas('status', 'order-created');
        $response->assertStatus(302);
    }

    public function test_orders_controller_store_no_rules(): void
    {
        $user = User::factory()->create();
        $product = Product::factory()->create();

        $response = $this
            ->actingAs($user)
            ->from('/admin/orders/create')
            ->post('/admin/orders', [
                'products' => [
                    [
                        'id' => $product->id,
                        'quantity' => 2,
                    ]
                ],
            ]);

        $response->assertStatus(403);
    }

    public function test_orders_controller_store_no_quantity(): void
    {
        $user = User::factory()->create();
        $product = Product::factory()->create();
        $user->syncPermissions(['orders_create']);

        $response = $this
            ->actingAs($user)
            ->from('/admin/orders/create')
            ->post('/admin/orders', [
                'products' => [
                    [
                        'id' => $product->id,
                    ]
                ],
            ]);

        $response->assertRedirectToRoute('orders.create');
        $response->assertInvalid(['products.0.quantity']);
        $response->assertStatus(302);
    }

    public function test_orders_controller_update_success(): void
    {
        $order = Order::factory()->create();
        $user = User::factory()->create();
        $product = Product::factory()->create();
        $user->syncPermissions(['orders_edit']);

        $response = $this
            ->actingAs($user)
            ->from('/admin/orders/' . $order->id . '/edit')
            ->patch('/admin/orders/' . $order->id, [
                'status_id' => 1,
                'products' => [
                    [
                        'id' => $product->id,
                        'quantity' => 2,
                    ]
                ],
            ]);

        $response->assertRedirectToRoute('orders.edit', $order->id);
        $response->assertSessionHas('status', 'order-updated');
        $response->assertStatus(302);
    }

    public function test_orders_controller_update_no_rules(): void
    {
        $order = Order::factory()->create();
        $user = User::factory()->create();
        $product = Product::factory()->create();

        $response = $this
            ->actingAs($user)
            ->from('/admin/orders/' . $order->id . '/edit')
            ->patch('/admin/orders/' . $order->id, [
                'status_id' => 1,
                'products' => [
                    [
                        'id' => $product->id,
                        'quantity' => 2,
                    ]
                ],
            ]);

        $response->assertStatus(403);
    }

    public function test_orders_controller_update_no_products_quantity(): void
    {
        $order = Order::factory()->create();
        $user = User::factory()->create();
        $product = Product::factory()->create();
        $user->syncPermissions(['orders_edit']);

        $response = $this
            ->actingAs($user)
            ->from('/admin/orders/' . $order->id . '/edit')
            ->patch('/admin/orders/' . $order->id, [
                'status_id' => 1,
                'products' => [
                    [
                        'id' => $product->id,
                    ]
                ],
            ]);

        $response->assertRedirectToRoute('orders.edit', $order->id);
        $response->assertInvalid(['products.0.quantity']);
        $response->assertStatus(302);
    }

    public function test_orders_controller_cancel_success(): void
    {
        $order = Order::factory()->create();
        $user = User::factory()->create();
        $user->syncPermissions(['orders_create']);

        $response = $this
            ->actingAs($user)
            ->from('/admin/orders/' . $order->id)
            ->post('/admin/orders/' . $order->id . '/cancel');

        $response->assertRedirectToRoute('orders.show', $order->id);
        $response->assertSessionHas('status', 'order-cancel');
        $response->assertStatus(302);
    }

    public function test_orders_controller_cancel_no_rules(): void
    {
        $order = Order::factory()->create();
        $user = User::factory()->create();

        $response = $this
            ->actingAs($user)
            ->from('/admin/orders/' . $order->id)
            ->post('/admin/orders/' . $order->id . '/cancel');

        $response->assertStatus(403);
    }

    public function test_orders_controller_repeat_success(): void
    {
        $order = Order::factory()->create();
        $user = User::factory()->create();
        $user->syncPermissions(['orders_create']);

        $response = $this
            ->actingAs($user)
            ->from('/admin/orders/' . $order->id)
            ->post('/admin/orders/' . $order->id . '/repeat');

        $response->assertRedirectToRoute('orders.show', $order->id + 1);
        $response->assertSessionHas('status', 'order-repeat');
        $response->assertStatus(302);
    }

    public function test_orders_controller_repeat_no_rules(): void
    {
        $order = Order::factory()->create();
        $user = User::factory()->create();

        $response = $this
            ->actingAs($user)
            ->from('/admin/orders/' . $order->id)
            ->post('/admin/orders/' . $order->id . '/repeat');

        $response->assertStatus(403);
    }

    public function test_orders_controller_destroy_success(): void
    {
        $order = Order::factory()->create();
        $user = User::factory()->create();
        $user->syncPermissions(['orders_edit']);

        $response = $this
            ->actingAs($user)
            ->from('/admin/orders/' . $order->id)
            ->delete('/admin/orders/' . $order->id);

        $response->assertRedirectToRoute('orders.index');
        $response->assertStatus(302);
    }

    public function test_orders_controller_destroy_no_rules(): void
    {
        $order = Order::factory()->create();
        $user = User::factory()->create();

        $response = $this
            ->actingAs($user)
            ->from('/admin/orders/' . $order->id)
            ->delete('/admin/orders/' . $order->id);

        $response->assertStatus(403);
    }
}
