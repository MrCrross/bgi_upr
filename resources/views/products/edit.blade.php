<x-app-layout>
    <x-slot
        name="header"
    >
        @include('products.partials.header')
    </x-slot>
    <div
        class="py-12"
    >
        <div
            class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6"
        >
            @if (count($errors) > 0)
                <div class="w-full px-10 py-5 bg-red-700">
                    <strong>{{ __('validation.whoops') }}</strong>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <section
                class="max-w-4xl p-6 mx-auto bg-white rounded-md shadow-md"
            >
                <form method="post" action="{{ route('products.admin.update', $product->id) }}" enctype="multipart/form-data">
                    @csrf
                    @method('patch')
                    <div
                        class="py-4"
                    >
                        <x-image-preview
                            src="{{$product->image}}"
                        />
                        <x-input-label
                            for="image"
                            :value="__('Изображение')"
                        />
                        <x-text-input
                            id="image"
                            name="image"
                            type="file"
                            accept="image/*"
                            class="use-ImagePreview mt-1 block w-full"
                        />
                        <x-input-error
                            class="mt-2"
                            :messages="$errors->get('image')"
                        />
                    </div>
                    <div
                        class="py-4"
                    >
                        <x-input-label
                            for="isbn"
                            :value="__('ISBN')"
                        />
                        <x-text-input
                            id="isbn"
                            name="isbn"
                            type="text"
                            class="mt-1 block w-full"
                            placeholder="0-000-00000-0"
                            :value="$product->isbn"
                            required
                        />
                        <x-input-error
                            class="mt-2"
                            :messages="$errors->get('isbn')"
                        />
                    </div>
                    <div
                        class="py-4"
                    >
                        <x-input-label
                            for="name"
                            :value="__('Название')"
                        />
                        <x-text-input
                            id="name"
                            name="name"
                            type="text"
                            class="mt-1 block w-full"
                            placeholder="Название"
                            :value="$product->name"
                            required
                        />
                        <x-input-error
                            class="mt-2"
                            :messages="$errors->get('name')"
                        />
                    </div>

                    <div
                        class="py-4"
                    >
                        <x-input-label
                            for="price"
                            :value="__('Стоимость')"
                        />
                        <x-text-input
                            id="price"
                            name="price"
                            type="number"
                            class="mt-1 block w-full"
                            min="0.00"
                            step="0.01"
                            :value="$product->price"
                            placeholder="100.00"
                            required
                        />
                        <x-input-error
                            class="mt-2"
                            :messages="$errors->get('price')"
                        />
                    </div>

                    <div
                        class="py-4"
                    >
                        <x-input-label
                            for="year_published"
                            :value="__('Год издания')"
                        />
                        <x-text-input
                            id="year_published"
                            name="year_published"
                            type="text"
                            class="mt-1 block w-full"
                            placeholder="2000"
                            :value="$product->year_published"
                            required
                        />
                        <x-input-error
                            class="mt-2"
                            :messages="$errors->get('year_published')"
                        />
                    </div>

                    <div
                        class="py-4"
                    >
                        <x-input-label
                            for="pages"
                            :value="__('Количество страниц')"
                        />
                        <x-text-input
                            id="pages"
                            name="pages"
                            type="number"
                            class="mt-1 block w-full"
                            min="1"
                            placeholder="50"
                            :value="$product->pages"
                            required
                        />
                        <x-input-error
                            class="mt-2"
                            :messages="$errors->get('pages')"
                        />
                    </div>

                    <div class="py-4">
                        <x-input-label
                            for="publisher_id"
                            :value="__('Издатель')"
                        />
                        <x-select
                            id="publisher_id"
                            name="publisher_id"
                            class="mt-1 block w-full"
                            :data="$publishers"
                            selected="{{$product->publisher_id}}"
                            required
                        />
                        <x-input-error
                            class="mt-2"
                            :messages="$errors->get('publisher_id')"
                        />
                    </div>
                    <div class="mt-4">
                        <h1 class="font-semibold text-xl text-gray-800 leading-tight">{{ __('authors.headers.title') }}</h1>
                        <div class="py-4 flex flex-col container-line-Author">
                            @if($product->authors->isNotEmpty())
                                @foreach($product->authors as $key => $author)
                                    <x-template.author :type="$key" :fieldID="$author->id" :authors="$authors"></x-template.author>
                                @endforeach
                            @else
                                <x-template.author :authors="$authors"></x-template.author>
                            @endif
                        </div>
                    </div>
                    <div class="mt-4">
                        <h1 class="font-semibold text-xl text-gray-800 leading-tight">{{ __('genres.headers.title') }}</h1>
                        <div class="py-4 flex flex-col container-line-Genre">
                            @if($product->genres->isNotEmpty())
                                @foreach($product->genres as $key => $genre)
                                    <x-template.genre :type="$key" :fieldID="$genre->id" :genres="$genres"></x-template.genre>
                                @endforeach
                            @else
                                <x-template.genre :genres="$genres"></x-template.genre>
                            @endif
                        </div>
                    </div>
                    <div
                        class="py-4"
                    >
                        <x-input-label
                            for="description"
                            :value="__('Описание')"
                        />
                        <x-textarea
                            id="description"
                            name="description"
                            class="mt-1 block w-full"
                            required
                        >
                            {{$product->description}}
                        </x-textarea>
                        <x-input-error
                            class="mt-2"
                            :messages="$errors->get('description')"
                        />
                    </div>
                    <div
                        class="flex items-center gap-4"
                    >
                        <x-primary-button>
                            {{ __('Сохранить') }}
                        </x-primary-button>

                        @if (session('status') === 'product-updated')
                            <p
                                x-data="{ show: true }"
                                x-show="show"
                                x-transition
                                x-init="setTimeout(() => show = false, 5000)"
                                class="text-sm text-gray-600"
                            >{{ __('Сохранено.') }}</p>
                        @endif
                    </div>
                </form>
            </section>
        </div>
    </div>
</x-app-layout>
