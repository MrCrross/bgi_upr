<x-app-layout>
    <x-slot name="header">
        @include('products.partials.header')
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
            <form method="GET" action="{{route('products.admin.index')}}"
                  class="rounded-xl shadow bg-gray-100 p-4">

                <h1 class="font-semibold text-xl text-gray-800 leading-tight">Фильтры</h1>
                <div class="grid grid-cols-2 gap-10">
                    <div class="">
                        <x-input-label
                            for="name"
                            :value="__('Название')"
                        />
                        <x-text-input
                            id="name"
                            name="name"
                            type="text"
                            :value="$products_filter->name ?? __('')"
                            class="mt-1 block w-full"
                        />
                    </div>
                    <div class="">
                        <x-input-label
                            for="price"
                            :value="__('Стоимость')"
                        />
                        <div class="flex flex-row justify-between items-center">
                            <x-input-label
                                for="min_price"
                                :value="__('от')"
                            />
                            <x-text-input
                                id="min_price"
                                name="min_price"
                                type="number"
                                min="1"
                                :value="$products_filter->min_price ?? __('1')"
                                class="mt-1"
                            />
                            <x-input-label
                                for="max_price"
                                :value="__('до')"
                            />
                            <x-text-input
                                id="max_price"
                                name="max_price"
                                type="number"
                                min="1"
                                :value="$products_filter->max_price ?? __('')"
                                class="mt-1"
                            />
                        </div>
                    </div>
                    <div class="">
                        <x-input-label
                            for="publisher_id"
                            :value="__('Издатель')"
                        />
                        <x-select
                            id="publisher_id"
                            name="publisher_id"
                            class="mt-1 block w-full"
                            :data="$publisher_select"
                            :selected="$products_filter->publisher_id ?? 0"
                        />
                    </div>
                </div>
                <h1 class="font-semibold text-xl text-gray-800 leading-tight my-2">Сортировка</h1>
                <div class="grid grid-cols-5 gap-10">
                    <div>
                        <x-input-label
                            for="order_name"
                            :value="__('Название')"
                        />
                        <x-select
                            id="order_name"
                            name="order_name"
                            class="mt-1"
                            :data="$products_order->default"
                            :selected="$products_order->name ?? 0"
                        />
                    </div>
                    <div>
                        <x-input-label
                            for="order_price"
                            :value="__('Стоимость')"
                        />
                        <x-select
                            id="order_price"
                            name="order_price"
                            class="mt-1"
                            :data="$products_order->default"
                            :selected="$products_order->price ?? 0"
                        />
                    </div>
                    <div>
                        <x-input-label
                            for="order_publisher_id"
                            :value="__('Издатель')"
                        />
                        <x-select
                            id="order_publisher_id"
                            name="order_publisher_id"
                            class="mt-1"
                            :data="$products_order->default"
                            :selected="$products_order->publisher_id ?? 0"
                        />
                    </div>
                </div>
                <div
                    class="flex items-center gap-4 mt-4"
                >
                    <x-primary-button>
                        {{ __('Применить') }}
                    </x-primary-button>
                </div>

            </form>
            <table class="table-auto w-full my-5">
                <tr>
                    <th class="w-1/12 border-2 border-gray-400 px-4 py-2">№</th>
                    <th class="w-1/12 border-2 border-gray-400 px-4 py-2">Обложка</th>
                    <th class="w-2/12 border-2 border-gray-400 px-4 py-2">Название</th>
                    <th class="w-1/12 border-2 border-gray-400 px-4 py-2">ISBN</th>
                    <th class="w-1/12 border-2 border-gray-400 px-4 py-2">Стоимость</th>
                    <th class="w-1/12 border-2 border-gray-400 px-4 py-2">Издатель</th>
                    <th class="w-2/12 border-2 border-gray-400 px-4 py-2">Статус</th>
                    <th class="w-3/12 border-2 border-gray-400 px-4 py-2">Действия</th>
                </tr>
                @if(!empty($products->items()))
                    @foreach($products as $product)
                        <tr>
                            <td class="border-2 border-gray-400 px-4 py-2">{{$product->id}}</td>
                            <td class="border-2 border-gray-400 px-4 py-2">
                                <img
                                    class="max-w-20"
                                    x-data=""
                                    x-on:click.prevent="$dispatch('open-modal', 'modal_Image{{$product->id}}')"
                                    src="{{$product->image}}"
                                    alt="{{$product->name}}"
                                >
                                <x-modal id="modal_Image{{$product->id}}" name="modal_Image{{$product->id}}" focusable>
                                    <div class="flex flex-row justify-center items-center m-4">
                                        <img
                                            src="{{$product->image}}"
                                            alt="{{$product->name}}"
                                        >
                                    </div>

                                    <div class="m-6 flex justify-end">
                                        <x-secondary-button x-on:click="$dispatch('close')">
                                            {{ __('Отмена') }}
                                        </x-secondary-button>
                                    </div>
                                </x-modal>
                            </td>
                            <td class="border-2 border-gray-400 px-4 py-2">{{$product->name}}</td>
                            <td class="border-2 border-gray-400 px-4 py-2">{{$product->isbn}}</td>
                            <td class="border-2 border-gray-400 px-4 py-2">{{$product->price}}</td>
                            <td class="border-2 border-gray-400 px-4 py-2">{{$product->publisher->name}}</td>
                            <td class="border-2 border-gray-400 px-4 py-2">
                                @if($product->status)
                                    <x-badge body="green">{{ __('publishers.statuses.success') }}</x-badge>
                                @else
                                    <x-badge body="red">{{ __('publishers.statuses.delete') }}</x-badge>
                                @endif
                            </td>
                            <td class="border-2 border-gray-400 px-4 py-2">
                                <x-a
                                    body="info"
                                    href="{{ route('products.admin.show', $product->id) }}"
                                >Посмотреть
                                </x-a>
                                @can('products_edit')
                                    @if($product->status)
                                        <x-a  href="{{ route('products.admin.edit', $product->id) }}">&#128393;</x-a>
                                        <form action="{{ route('products.admin.destroy', $product->id) }}" method="POST" class="inline-block">
                                            @csrf
                                            @method('DELETE')
                                            <x-btn body="danger" type="submit">&times;</x-btn>
                                        </form>
                                    @endif
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="7"><x-no-data></x-no-data></td>
                    </tr>
                @endif
            </table>
            <x-paginate :paginator="$products"></x-paginate>
        </div>
    </div>
</x-app-layout>
