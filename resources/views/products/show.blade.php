<x-app-layout>
    <x-slot name="header">
        @include('products.partials.header')
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
            <div
                class="max-w-lg flex flex-col justify-center items-center py-4 lg:max-w-none lg:p-6 bg-white shadow-xl sm:rounded-lg">
                <div class="flex flex-col justify-center items-start gap-2">
                    <h2 class="text-2xl font-medium text-gray-900">{{$product->name}}</h2>
                    <div class="flex flex-row justify-between">
                        <div class="py-2 pl-2 pr-4 rounded-2xl">
                            <img
                                class="max-w-lg rounded-2xl"
                                x-data=""
                                x-on:click.prevent="$dispatch('open-modal', 'modal_Image{{$product->id}}')"
                                src="{{$product->image}}"
                                alt="{{$product->name}}"
                            >
                            <x-modal id="modal_Image{{$product->id}}" name="modal_Image{{$product->id}}" focusable>
                                <div class="flex flex-row justify-center items-center m-4">
                                    <img
                                        src="{{$product->image}}"
                                        alt="{{$product->name}}"
                                    >
                                </div>

                                <div class="m-6 flex justify-end">
                                    <x-secondary-button x-on:click="$dispatch('close')">
                                        {{ __('Отмена') }}
                                    </x-secondary-button>
                                </div>
                            </x-modal>
                        </div>
                        <div class="py-2 pr-2 pl-4">
                            <div class="flex flex-col">
                                <strong>ISBN:</strong>
                                {{ $product->isbn }}
                            </div>
                            <div class="flex flex-col">
                                <strong>Стоимость:</strong>
                                {{ $product->price }} руб.
                            </div>
                            <div class="flex flex-col">
                                <strong>Количество страниц:</strong>
                                {{ $product->price }}
                            </div>
                            <div class="flex flex-col">
                                <strong>Год публикации:</strong>
                                {{ $product->year_published }}
                            </div>
                            <div class="flex flex-col">
                                <strong>Издатель:</strong>
                                {{$product->publisher->name}}
                            </div>
                            <div class="flex flex-col">
                                <strong>Авторы:</strong>
                                @foreach($product->authors as $author)
                                    <p>{{$author->fio}}</p>
                                @endforeach
                            </div>
                            <div class="flex flex-col">
                                <strong>Жанры:</strong>
                                @foreach($product->genres as $genre)
                                    <p>{{$genre->name}}</p>
                                @endforeach
                            </div>
                            <div class="flex flex-col">
                                <strong>Описание:</strong>
                                {{ $product->description }}
                            </div>
                        </div>
                    </div>
                    <div class="flex flex-row w-full justify-center items-center gap-2">
                        @can('products_edit')
                            <x-primary-a
                                :href="route('products.admin.edit', $product->id)">{{__('Редактировать')}}</x-primary-a>
                        @endcan
                        @if(!$product->status)
                            @can('products_edit')
                                <div class="my-3">
                                    <form method="POST" action="{{route('products.admin.recovery', $product->id)}}">
                                        @csrf
                                        @method('PATCH')
                                        <x-primary-button>{{ __('actions.recovery') }}</x-primary-button>
                                    </form>
                                </div>
                            @endcan
                        @else
                            @can('products_delete')
                                <x-danger-button
                                    type="submit"
                                    x-data=""
                                    x-on:click.prevent="$dispatch('open-modal', 'confirm-product-deletion')"
                                >
                                    {{__('Удалить')}}
                                </x-danger-button>
                            @endcan
                        @endif
                    </div>
                </div>
                <x-history-table :history="$history"></x-history-table>
            </div>
        </div>
    </div>

    <x-modal name="confirm-product-deletion" focusable>
        <form method="post" action="{{ route('products.admin.destroy', $product->id) }}" class="p-6">
            @csrf
            @method('delete')

            <h2 class="text-lg font-medium text-gray-900">
                {{ __('Вы уверены, что хотите удалить товар "' . $product->name . '"?') }}
            </h2>

            <div class="mt-6 flex justify-end">
                <x-secondary-button x-on:click="$dispatch('close')">
                    {{ __('Отмена') }}
                </x-secondary-button>

                <x-danger-button class="ml-3" type="submit">
                    {{ __('Удалить') }}
                </x-danger-button>
            </div>
        </form>
    </x-modal>
</x-app-layout>
