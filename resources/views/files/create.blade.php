<x-app-layout>
    <x-slot name="header">
        <div class="mb-5">
            <div class="float-left">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">{{ __('files.headers.create') }}</h2>
            </div>
            <div class="float-right">
                <x-a href="{{ route('files.index') }}">{{ __('actions.back') }}</x-a>
            </div>
        </div>
    </x-slot>
    @if (count($errors) > 0)
        <div class="w-full px-10 py-5 bg-red-700">
            <strong>{{ __('validation.whoops') }}</strong>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="container mx-auto my-5 bg-gray-50 rounded">
        <div class="py-5 mx-5 flex flex-col justify-center items-center">
            <form action="{{route('files.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="my-5 mx-5">
                    <div class="flex flex-col">
                        <x-input-label for="file" :value="__('files.fields.title')" />
                        <x-text-input type="text" name="title" :placeholder="__('files.fields.title')"></x-text-input>
                    </div>
                    <div
                        class="py-4"
                    >
                        <x-input-label
                            for="file"
                            :value="__('files.fields.path')"
                        />
                        <x-text-input
                            id="file"
                            name="file"
                            type="file"
                            class="mt-1 block w-full"
                            required
                        />
                        <x-input-error
                            class="mt-2"
                            :messages="$errors->get('file')"
                        />
                    </div>
                    <div class="mt-2">
                        <x-btn type="submit">{{ __('actions.submit') }}</x-btn>
                    </div>
                </div>
            </form>
        </div>
    </div>
</x-app-layout>
