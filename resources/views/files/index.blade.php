<x-app-layout>

    <x-slot name="header">
        <div class="mb-5">
            <div class="float-left">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight ">{{ __('files.headers.title') }}</h2>
            </div>
            <div class="float-right">
                @can('files_edit')
                    <x-a body="success" href="{{ route('files.create') }}">{{ __('files.headers.create') }}</x-a>
                @endcan
            </div>
        </div>
    </x-slot>


@if ($message = Session::get('success'))
    <div class="w-full px-10 py-5 bg-green-500" >
        <p>{{ $message }}</p>
    </div>
@endif

<div class="container mx-auto px-4 my-5">
    <form method="GET" action="{{route('files.index')}}"
          class="rounded-xl shadow bg-gray-100 p-4">
        @csrf
        <h1 class="font-semibold text-xl text-gray-800 leading-tight">{{ __('datatable.filters') }}</h1>
        <div class="grid grid-cols-3 gap-10">
            <div class="">
                <x-input-label
                    for="title"
                    :value="__('files.fields.title')"
                />
                <x-text-input
                    id="title"
                    name="title"
                    type="text"
                    :value="$filter->title ?? __('')"
                    class="mt-1 block w-full"
                />
            </div>
            <div class="">
                <x-input-label
                    for="creator_id"
                    :value="__('files.fields.creator_id')"
                />
                <x-select
                    id="creator_id"
                    name="creator_id"
                    class="mt-1"
                    :data="$users_select"
                    :selected="$filter->creator_id ?? 0"
                />
            </div>
        </div>
        <h1 class="font-semibold text-xl text-gray-800 leading-tight my-2">{{ __('datatable.sorting') }}</h1>
        <div class="grid grid-cols-5 gap-10">
            <div>
                <x-input-label
                    for="order_title"
                    :value="__('files.fields.title')"
                />
                <x-select
                    id="order_title"
                    name="order_title"
                    class="mt-1"
                    :data="$order->default"
                    :selected="$order->title ?? 0"
                />
            </div>
            <div>
                <x-input-label
                    for="order_creator_id"
                    :value="__('files.fields.creator_id')"
                />
                <x-select
                    id="order_creator_id"
                    name="order_creator_id"
                    class="mt-1"
                    :data="$order->default"
                    :selected="$order->creator_id ?? 0"
                />
            </div>
        </div>
        <div
            class="flex items-center gap-4 mt-4"
        >
            <x-primary-button>
                {{ __('actions.apply') }}
            </x-primary-button>
        </div>
    </form>
    <table class="table-auto w-full my-5">
        <thead>
        <tr>
            <th class="w-1/12 border-2 border-gray-400 px-4 py-2">№</th>
            <th class="w-4/12 border-2 border-gray-400 px-4 py-2">{{ __('files.fields.title') }}</th>
            <th class="w-4/12 border-2 border-gray-400 px-4 py-2">{{ __('files.fields.path') }}</th>
            <th class="w-2/12 border-2 border-gray-400 px-4 py-2">{{__('files.fields.status')}}</th>
            <th class="w-4/12 border-2 border-gray-400 px-4 py-2">{{ __('datatable.action') }}</th>
        </tr>
        </thead>
        <tbody>
        @if(!empty($files->items()))
            @php $pageCount = ($files->currentPage() * $files->perPage()) - $files->perPage(); @endphp
            @foreach ($files as $key => $file)
                <tr>
                    <td class="border-2 border-gray-400 px-4 py-2">{{ ++$key + $pageCount }}</td>
                    <td class="border-2 border-gray-400 px-4 py-2">{{ $file->title }}</td>
                    <td class="border-2 border-gray-400 px-4 py-2"><x-a href="{{asset($file->path)}}" target="_blank">{{ __('actions.download') }}</x-a></td>
                    <td class="border-2 border-gray-400 px-4 py-2">
                        @if($file->status)
                            <x-badge body="green">{{ __('files.statuses.success') }}</x-badge>
                        @else
                            <x-badge body="red">{{ __('files.statuses.delete') }}</x-badge>
                        @endif
                    </td>
                    <td class="border-2 border-gray-400 px-4 py-2">
                        <x-a body="info" href="{{ route('files.show', $file->id) }}">{{ __('actions.view') }}</x-a>
                        @can('files_edit')
                            @if($file->status)
                                <x-a  href="{{ route('files.edit', $file->id) }}">&#128393;</x-a>
                                <form action="{{ route('files.destroy', $file->id) }}" method="POST" class="inline-block">
                                    @csrf
                                    @method('DELETE')
                                    <x-btn body="danger" type="submit">&times;</x-btn>
                                </form>
                            @endif
                        @endcan
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="5"><x-no-data></x-no-data></td>
            </tr>
        @endif
        </tbody>
    </table>
    <x-paginate :paginator="$files"></x-paginate>
</div>
</x-app-layout>
