<x-app-layout>
    <x-slot name="header">
        <div class="mb-5">
            <div class="float-left">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight ">{{ __('files.headers.view') }}</h2>
            </div>
            <div class="float-right">
                <x-a href="{{ route('files.index') }}">{{ __('actions.back') }}</x-a>
            </div>
        </div>
    </x-slot>
    <div class="container mx-auto my-5 bg-gray-50 rounded flex flex-col justify-center items-center">
        @if ($message = Session::get('success'))
            <div class="w-full px-10 py-5 bg-green-500">
                <p>{{ $message }}</p>
            </div>
        @endif
        <div class="py-5 mx-5">
            <div class="flex flex-col">
                <strong>{{ __('files.fields.title') }}</strong>
                {{ $file->title }}
            </div>
            <div class="flex flex-col">
                <strong>{{ __('files.fields.path') }}</strong>
                <x-a href="{{ asset($file->path) }}" target="_blank">{{ __('actions.download') }}</x-a>
            </div>
            @if(!$file->status)
                @can('files_edit')
                    <div class="my-3">
                        <form method="POST" action="{{route('files.recovery', $file->id)}}">
                            @csrf
                            @method('PATCH')
                            <x-primary-button>{{ __('actions.recovery') }}</x-primary-button>
                        </form>
                    </div>
                @endcan
            @endif
        </div>
        <x-history-table :history="$history"></x-history-table>
    </div>
</x-app-layout>
