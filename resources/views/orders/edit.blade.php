<x-app-layout>
    <x-slot
        name="header"
    >
        @include('orders.partials.header')
    </x-slot>
    <div
        class="py-12"
    >
        <div
            class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6"
        >
            @if (count($errors) > 0)
                <div class="w-full px-10 py-5 bg-red-700">
                    <strong>{{ __('validation.whoops') }}</strong>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <x-order-product-form key="0" clone="1" :products="$products"></x-order-product-form>
            <section
                class="max-w-4xl p-6 mx-auto bg-white rounded-md shadow-md"
            >
                <form method="post" action="{{ route('orders.update', $order->id) }}">
                    @csrf
                    @method('patch')
                    <h1 class="font-semibold text-xl text-gray-800 leading-tight">
                        <p>№{{$order->id}}</p>
                        <p>Дата: {{\Illuminate\Support\Carbon::parse($order->date)->format('d.m.Y H:i')}}</p>
                        <p>Статус: {{$order->status->name}}</p>
                        <p>Создал: {{ $order->customer->name}}</p>
                    </h1>
                    <div class="py-4">
                        <x-input-label
                            for="status_id"
                            :value="__('Статус')"
                        />
                        <x-select
                            id="status_id"
                            name="status_id"
                            class="mt-1 block w-full"
                            :data="$statuses"
                            :selected="$order->status_id"
                            required
                        />
                        <x-input-error
                            class="mt-2"
                            :messages="$errors->get('status_id')"
                        />
                    </div>
                    <h1 class="font-semibold text-xl text-gray-800 leading-tight">Товары</h1>
                    <div class="container-line-ProductOrder">
                        @foreach($order->products as $key => $product)
                            <x-order-product-form
                                :type="$key"
                                :key="$key"
                                :products="$products"
                                :productCount="$product->quantity"
                                :productID="$product->product_id"
                            ></x-order-product-form>
                        @endforeach
                    </div>
                    <div
                        class="flex items-center gap-4"
                    >
                        <x-primary-button>
                            {{ __('Сохранить') }}
                        </x-primary-button>
                        <span id="order_price" class="pl-5">0</span><span class="pl-5">руб.</span>
                        @if (session('status') === 'order-updated')
                            <p
                                x-data="{ show: true }"
                                x-show="show"
                                x-transition
                                x-init="setTimeout(() => show = false, 5000)"
                                class="text-sm text-gray-600"
                            >{{ __('Сохранено.') }}</p>
                        @endif
                    </div>
                </form>
            </section>
        </div>
    </div>
</x-app-layout>
<script src="{{asset('/js/templates/OrderForm.js')}}"></script>
<script>
    document.addEventListener('DOMContentLoaded', () => {
        new OrderForm();
    })
</script>
