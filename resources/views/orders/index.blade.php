<x-app-layout>
    <x-slot name="header">
        @include('orders.partials.header')
    </x-slot>
    <div class="py-12">
        <div class="container mx-auto px-4 my-5">
            <form method="GET" action="{{route('orders.index')}}"
                  class="rounded-xl shadow bg-gray-100 p-4">
                @csrf
                <h1 class="font-semibold text-xl text-gray-800 leading-tight">{{ __('datatable.filters') }}</h1>
                <div class="grid grid-cols-3 gap-10">
                    <div class="">
                        <x-input-label
                            for="customer_id"
                            :value="__('Составил')"
                        />
                        <x-select
                            id="customer_id"
                            name="customer_id"
                            class="mt-1 block w-full"
                            :data="$users_select"
                            :selected="$orders_filter->customer_id ?? 0"
                        />
                    </div>
                    <div class="">
                        <x-input-label
                            for="date"
                            :value="__('Дата заказа')"
                        />
                        <div class="flex flex-row justify-between items-center">
                            <x-input-label
                                for="min_date"
                                :value="__('от')"
                            />
                            <x-text-input
                                id="min_date"
                                name="min_date"
                                type="datetime-local"
                                min="1"
                                :value="$orders_filter->min_date ?? null"
                                class="mt-1"
                            />
                            <x-input-label
                                for="max_date"
                                :value="__('до')"
                            />
                            <x-text-input
                                id="max_date"
                                name="max_date"
                                type="datetime-local"
                                min="1"
                                :value="$orders_filter->max_date ?? null"
                                class="mt-1"
                            />
                        </div>
                    </div>
                </div>
                <h1 class="font-semibold text-xl text-gray-800 leading-tight my-2">{{ __('datatable.sorting') }}</h1>
                <div class="grid grid-cols-5 gap-10">
                    <div>
                        <x-input-label
                            for="order_customer_id"
                            :value="__('Составил')"
                        />
                        <x-select
                            id="order_customer_id"
                            name="order_customer_id"
                            class="mt-1"
                            :data="$orders_order->default"
                            :selected="$orders_order->customer_id ?? 0"
                        />
                    </div>
                    <div>
                        <x-input-label
                            for="order_date"
                            :value="__('Дата заказа')"
                        />
                        <x-select
                            id="order_date"
                            name="order_date"
                            class="mt-1"
                            :data="$orders_order->default"
                            :selected="$orders_order->date ?? 0"
                        />
                    </div>
                </div>
                <div
                    class="flex items-center gap-4 mt-4"
                >
                    <x-primary-button>
                        {{ __('Применить') }}
                    </x-primary-button>
                </div>
            </form>
            <table class="table-auto w-full my-5">
                <tr>
                    <th class="w-1/12 border-2 border-gray-400 px-4 py-2">№</th>
                    <th class="w-2/12 border-2 border-gray-400 px-4 py-2">Дата</th>
                    <th class="w-3/12 border-2 border-gray-400 px-4 py-2">Состав</th>
                    <th class="w-2/12 border-2 border-gray-400 px-4 py-2">Статус</th>
                    <th class="w-2/12 border-2 border-gray-400 px-4 py-2">Составил</th>
                    <th class="w-2/12 border-2 border-gray-400 px-4 py-2">Действия</th>
                </tr>
                @if(!empty($orders->items()))
                    @foreach($orders as $order)
                        <tr>
                            <td class="border-2 border-gray-400 px-4 py-2">{{$order->id}}</td>
                            <td class="border-2 border-gray-400 px-4 py-2">{{\Illuminate\Support\Carbon::parse($order->date)->format('d.m.Y H:i')}}</td>
                            <td class="border-2 border-gray-400 px-4 py-2">
                                @foreach($order->products as $product)
                                    <div class="text-gray-800">
                                        <span>
                                            {{$product->quantity}} x {{$product->product->name}}
                                        </span>
                                    </div>
                                @endforeach
                            </td>
                            <td class="border-2 border-gray-400 px-4 py-2">
                                @if(empty($order->deleted_at))
                                    {{$order->status->name}}
                                @else
                                    Удалено
                                @endif
                            </td>
                            <td class="border-2 border-gray-400 px-4 py-2">
                                {{ $order->customer->name }}
                            </td>
                            <td class="border-2 border-gray-400 px-4 py-2">
                                <x-a
                                    body="info"
                                    href="{{ route('orders.show', $order->id) }}"
                                >Посмотреть
                                </x-a>
                                @can('orders_edit')
                                    <x-a href="{{ route('orders.edit', $order->id) }}">&#128393;</x-a>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="6"><x-no-data></x-no-data></td>
                    </tr>
                @endif
            </table>
            <x-paginate :paginator="$orders"></x-paginate>
        </div>
    </div>
</x-app-layout>
