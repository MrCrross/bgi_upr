<x-app-layout>
    <x-slot name="header">
        @include('news.partials.header')
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
            <form method="GET" action="{{route('news.admin.index')}}"
                  class="rounded-xl shadow bg-gray-100 p-4">

                <h1 class="font-semibold text-xl text-gray-800 leading-tight">Фильтры</h1>
                <div class="grid grid-cols-2 gap-10">
                    <div class="">
                        <x-input-label
                            for="title"
                            :value="__('Заголовок')"
                        />
                        <x-text-input
                            id="title"
                            name="title"
                            type="text"
                            :value="$news_filter->title ?? __('')"
                            class="mt-1 block w-full"
                        />
                    </div>
                </div>
                <h1 class="font-semibold text-xl text-gray-800 leading-tight my-2">Сортировка</h1>
                <div class="grid grid-cols-5 gap-10">
                    <div>
                        <x-input-label
                            for="order_title"
                            :value="__('Заголовок')"
                        />
                        <x-select
                            id="order_title"
                            name="order_title"
                            class="mt-1"
                            :data="$news_order->default"
                            :selected="$news_order->title ?? 0"
                        />
                    </div>
                </div>
                <div
                    class="flex items-center gap-4 mt-4"
                >
                    <x-primary-button>
                        {{ __('Применить') }}
                    </x-primary-button>
                </div>

            </form>
            <table class="table-auto w-full my-5">
                <tr>
                    <th class="w-1/12 border-2 border-gray-400 px-4 py-2">№</th>
                    <th class="w-1/12 border-2 border-gray-400 px-4 py-2">Обложка</th>
                    <th class="w-2/12 border-2 border-gray-400 px-4 py-2">Заголовок</th>
                    <th class="w-2/12 border-2 border-gray-400 px-4 py-2">Дата новости</th>
                    <th class="w-2/12 border-2 border-gray-400 px-4 py-2">Создатель</th>
                    <th class="w-1/12 border-2 border-gray-400 px-4 py-2">Статус</th>
                    <th class="w-3/12 border-2 border-gray-400 px-4 py-2">Действия</th>
                </tr>
                @if(!empty($news->items()))
                    @foreach($news as $item)
                        <tr>
                            <td class="border-2 border-gray-400 px-4 py-2">{{$item->id}}</td>
                            <td class="border-2 border-gray-400 px-4 py-2">
                                <img
                                    class="max-w-20"
                                    x-data=""
                                    x-on:click.prevent="$dispatch('open-modal', 'modal_Image{{$item->id}}')"
                                    src="{{$item->image}}"
                                    alt="{{$item->title}}"
                                >
                                <x-modal id="modal_Image{{$item->id}}" name="modal_Image{{$item->id}}" focusable>
                                    <div class="flex flex-row justify-center items-center m-4">
                                        <img
                                            src="{{$item->image}}"
                                            alt="{{$item->title}}"
                                        >
                                    </div>

                                    <div class="m-6 flex justify-end">
                                        <x-secondary-button x-on:click="$dispatch('close')">
                                            {{ __('Отмена') }}
                                        </x-secondary-button>
                                    </div>
                                </x-modal>
                            </td>
                            <td class="border-2 border-gray-400 px-4 py-2">{{$item->title}}</td>
                            <td class="border-2 border-gray-400 px-4 py-2">{{\Carbon\Carbon::parse($item->date)->format('d.m.Y h:i:s')}}</td>
                            <td class="border-2 border-gray-400 px-4 py-2">{{$item->creator->name}}</td>
                            <td class="border-2 border-gray-400 px-4 py-2">
                                @if($item->status)
                                    <x-badge body="green">{{ __('publishers.statuses.success') }}</x-badge>
                                @else
                                    <x-badge body="red">{{ __('publishers.statuses.delete') }}</x-badge>
                                @endif
                            </td>
                            <td class="border-2 border-gray-400 px-4 py-2">
                                <x-a
                                    body="info"
                                    href="{{ route('news.admin.show', $item->id) }}"
                                >Посмотреть
                                </x-a>
                                @can('news_edit')
                                    @if($item->status)
                                        <x-a  href="{{ route('news.admin.edit', $item->id) }}">&#128393;</x-a>
                                        <form action="{{ route('news.admin.destroy', $item->id) }}" method="POST" class="inline-block">
                                            @csrf
                                            @method('DELETE')
                                            <x-btn body="danger" type="submit">&times;</x-btn>
                                        </form>
                                    @endif
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="7"><x-no-data></x-no-data></td>
                    </tr>
                @endif
            </table>
            <x-paginate :paginator="$news"></x-paginate>
        </div>
    </div>
</x-app-layout>
