<x-app-layout>
    <x-slot
        name="header"
    >
        @include('news.partials.header')
    </x-slot>
    <div
        class="py-12"
    >
        <div
            class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6"
        >
            @if (count($errors) > 0)
                <div class="w-full px-10 py-5 bg-red-700">
                    <strong>{{ __('validation.whoops') }}</strong>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <section
                class="max-w-4xl p-6 mx-auto bg-white rounded-md shadow-md"
            >
                <form method="post" action="{{ route('news.admin.update', $news->id) }}" enctype="multipart/form-data">
                    @csrf
                    @method('patch')
                    <div
                        class="py-4"
                    >
                        <x-image-preview
                            src="{{$news->image}}"
                        />
                        <x-input-label
                            for="image"
                            :value="__('Обложка')"
                        />
                        <x-text-input
                            id="image"
                            name="image"
                            type="file"
                            accept="image/*"
                            class="use-ImagePreview mt-1 block w-full"
                        />
                        <x-input-error
                            class="mt-2"
                            :messages="$errors->get('image')"
                        />
                    </div>
                    <div
                        class="py-4"
                    >
                        <x-input-label
                            for="title"
                            :value="__('Заголовок')"
                        />
                        <x-text-input
                            id="title"
                            name="title"
                            type="text"
                            class="mt-1 block w-full"
                            placeholder="Заголовок"
                            :value="$news->title"
                            required
                        />
                        <x-input-error
                            class="mt-2"
                            :messages="$errors->get('title')"
                        />
                    </div>
                    <div
                        class="py-4"
                    >
                        <x-input-label
                            for="content"
                            :value="__('Содержание')"
                        />
                        <x-textarea
                            id="content"
                            name="content"
                            class="mt-1 block w-full"
                            required
                        >
                            {{$news->content}}
                        </x-textarea>
                        <x-input-error
                            class="mt-2"
                            :messages="$errors->get('content')"
                        />
                    </div>
                    <div
                        class="flex items-center gap-4"
                    >
                        <x-primary-button>
                            {{ __('Сохранить') }}
                        </x-primary-button>

                        @if (session('status') === 'news-updated')
                            <p
                                x-data="{ show: true }"
                                x-show="show"
                                x-transition
                                x-init="setTimeout(() => show = false, 5000)"
                                class="text-sm text-gray-600"
                            >{{ __('Сохранено.') }}</p>
                        @endif
                    </div>
                </form>
            </section>
        </div>
    </div>
</x-app-layout>
