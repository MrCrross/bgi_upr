<x-app-layout>
    <x-slot name="header">
        @include('news.partials.header')
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
            <div
                class="max-w-lg flex flex-col justify-center items-center py-4 lg:max-w-none lg:p-6 bg-white shadow-xl sm:rounded-lg">
                <div class="flex flex-col justify-center items-start gap-2">
                    <h2 class="text-2xl font-medium text-gray-900">{{$news->title}}</h2>
                    <div class="flex flex-row justify-between">
                        <div class="py-2 pl-2 pr-4 rounded-2xl">
                            <img
                                class="max-w-lg rounded-2xl"
                                x-data=""
                                x-on:click.prevent="$dispatch('open-modal', 'modal_Image{{$news->id}}')"
                                src="{{$news->image}}"
                                alt="{{$news->title}}"
                            >
                            <x-modal id="modal_Image{{$news->id}}" name="modal_Image{{$news->id}}" focusable>
                                <div class="flex flex-row justify-center items-center m-4">
                                    <img
                                        src="{{$news->image}}"
                                        alt="{{$news->title}}"
                                    >
                                </div>

                                <div class="m-6 flex justify-end">
                                    <x-secondary-button x-on:click="$dispatch('close')">
                                        {{ __('Отмена') }}
                                    </x-secondary-button>
                                </div>
                            </x-modal>
                        </div>
                        <div class="py-2 pr-2 pl-4">
                            <div class="flex flex-col">
                                <strong>Дата новости:</strong>
                                {{\Carbon\Carbon::parse($news->date)->format('d.m.Y h:i:s')}}
                            </div>
                            <div class="flex flex-col">
                                <strong>Создатель:</strong>
                                {{$news->creator->name}}
                            </div>
                            <div class="flex flex-col">
                                <strong>Содержание:</strong>
                                {{ $news->content }}
                            </div>
                        </div>
                    </div>
                    <div class="flex flex-row w-full justify-center items-center gap-2">
                        @can('news_edit')
                            <x-primary-a
                                :href="route('news.admin.edit', $news->id)">{{__('Редактировать')}}</x-primary-a>
                        @endcan
                        @can('news_edit')
                            @if(!$news->status)
                                <div class="my-3">
                                    <form method="POST" action="{{route('news.admin.recovery', $news->id)}}">
                                        @csrf
                                        @method('PATCH')
                                        <x-primary-button>{{ __('actions.recovery') }}</x-primary-button>
                                    </form>
                                </div>
                            @else
                                <x-danger-button
                                    type="submit"
                                    x-data=""
                                    x-on:click.prevent="$dispatch('open-modal', 'confirm-product-deletion')"
                                >
                                    {{__('Удалить')}}
                                </x-danger-button>
                            @endif
                        @endcan
                    </div>
                </div>
                <x-history-table :history="$history"></x-history-table>
            </div>
        </div>
    </div>

    <x-modal name="confirm-product-deletion" focusable>
        <form method="post" action="{{ route('news.admin.destroy', $news->id) }}" class="p-6">
            @csrf
            @method('delete')

            <h2 class="text-lg font-medium text-gray-900">
                {{ __('Вы уверены, что хотите удалить новость "' . $news->title . '"?') }}
            </h2>

            <div class="mt-6 flex justify-end">
                <x-secondary-button x-on:click="$dispatch('close')">
                    {{ __('Отмена') }}
                </x-secondary-button>

                <x-danger-button class="ml-3" type="submit">
                    {{ __('Удалить') }}
                </x-danger-button>
            </div>
        </form>
    </x-modal>

</x-app-layout>
