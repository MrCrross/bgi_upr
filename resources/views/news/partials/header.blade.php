<h2 class="font-semibold text-xl text-gray-800 leading-tight">
    <div class="flex flex-row items-center justify-between">
        <h1>{{ __('Новости') }}</h1>
        @auth
            <div class="flex flex-row items-center justify-between">
                @can('news_edit')
                    <x-nav-link :href="route('news.admin.create')" :active="request()->routeIs('news.admin.create')">
                        {{__('Добавить')}}
                    </x-nav-link>
                @endcan
            </div>
        @endauth
    </div>
</h2>
