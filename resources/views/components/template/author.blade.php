<div
    @php if(!empty($clone)) { echo 'id="line-clone-Author"'; } @endphp
    class="flex flex-row justify-between items-end py-4 line-Author {{empty($clone) ? '' : 'hidden'}}"
>
    <div>
        <x-input-label
            for="author_id"
            :value="__('authors.headers.single')"
        />
        <x-select
            id="author_id"
            name="authors[]"
            class="mt-1 block w-full"
            :data="$authors"
            :selected="!empty($fieldID) ? $fieldID : 0"
            required
        />
    </div>

    <div class="flex flex-row justify-end items-end">
        <x-btn
            body="success"
            type="button"
            class="add-line-Author {{empty($type) ? '' : 'hidden'}}"
        >
            {{ __('+') }}
        </x-btn>
        <x-btn
            body="danger"
            type="button"
            class="remove-line-Author {{!empty($type) ? '' : 'hidden'}}"
        >
            {{ __('-') }}
        </x-btn>
    </div>
</div>
