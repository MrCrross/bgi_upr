<div
    @php if(!empty($clone)) { echo 'id="line-clone-Genre"'; } @endphp
    class="flex flex-row justify-between items-end py-4 line-Genre {{empty($clone) ? '' : 'hidden'}}"
>
    <div>
        <x-input-label
            for="genre_id"
            :value="__('genres.headers.single')"
        />
        <x-select
            id="genre_id"
            name="genres[]"
            class="mt-1 block w-full"
            :data="$genres"
            :selected="!empty($fieldID) ? $fieldID : 0"
            required
        />
    </div>

    <div class="flex flex-row justify-end items-end">
        <x-btn
            body="success"
            type="button"
            class="add-line-Genre {{empty($type) ? '' : 'hidden'}}"
        >
            {{ __('+') }}
        </x-btn>
        <x-btn
            body="danger"
            type="button"
            class="remove-line-Genre {{!empty($type) ? '' : 'hidden'}}"
        >
            {{ __('-') }}
        </x-btn>
    </div>
</div>
