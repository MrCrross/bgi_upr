class GenreTemplate {
    static btnAddLineSelector = '.add-line-Genre';
    static btnRemoveLineSelector = '.remove-line-Genre';
    static containerLineSelector = '.container-line-Genre';
    static lineGenreClass = 'line-Genre';
    static lineGenreCloneSelector = '#line-clone-Genre';

    constructor() {
        GenreTemplate.addListeners();
    }

    static addListeners()
    {
        const addBtns = document.querySelectorAll(GenreTemplate.btnAddLineSelector);
        const removeBtns = document.querySelectorAll(GenreTemplate.btnRemoveLineSelector);
        const inputCheckBox = document.querySelectorAll('.input_checkbox');

        if (addBtns) {
            addBtns.forEach((btn) => {
                btn.addEventListener('click', (event) => GenreTemplate.addNewLine(event));
            });
        }
        if (removeBtns) {
            removeBtns.forEach((btn) => {
                btn.addEventListener('click', (event) => GenreTemplate.removeLine(event));
            });
        }
        if (inputCheckBox) {
            inputCheckBox.forEach((checkBox) => {
                checkBox.addEventListener('change', (event) => {
                    const target = event.target;
                    if (target.checked) {
                        target.value = 1;
                    } else {
                        target.value = 0;
                    }
                });
            })
        }
    }

    static addNewLine(event)
    {
        const clone = GenreTemplate.getClone();
        const container = document.querySelector(GenreTemplate.containerLineSelector);
        container.append(clone);
    }

    static removeLine(event)
    {
        const line = GenreTemplate.getLineGenre(event.target);
        line.remove()
    }

    static getLineGenre(element)
    {
        while(element = element.parentElement) {
            if (element.classList.contains(GenreTemplate.lineGenreClass)) {
                return element;
            }
        }
    }

    static getClone()
    {
        const clone = document.querySelector(GenreTemplate.lineGenreCloneSelector).cloneNode(true);
        const addBtn = clone.querySelector(GenreTemplate.btnAddLineSelector);
        const removeBtn = clone.querySelector(GenreTemplate.btnRemoveLineSelector);
        clone.id = '';
        clone.classList.remove('hidden');
        addBtn.remove();
        removeBtn.addEventListener('click', (event) => GenreTemplate.removeLine(event));
        removeBtn.classList.remove('hidden');

        return clone;
    }
}
