class AuthorTemplate {
    static btnAddLineSelector = '.add-line-Author';
    static btnRemoveLineSelector = '.remove-line-Author';
    static containerLineSelector = '.container-line-Author';
    static lineAuthorClass = 'line-Author';
    static lineAuthorCloneSelector = '#line-clone-Author';

    constructor() {
        AuthorTemplate.addListeners();
    }

    static addListeners()
    {
        const addBtns = document.querySelectorAll(AuthorTemplate.btnAddLineSelector);
        const removeBtns = document.querySelectorAll(AuthorTemplate.btnRemoveLineSelector);
        const inputCheckBox = document.querySelectorAll('.input_checkbox');

        if (addBtns) {
            addBtns.forEach((btn) => {
                btn.addEventListener('click', (event) => AuthorTemplate.addNewLine(event));
            });
        }
        if (removeBtns) {
            removeBtns.forEach((btn) => {
                btn.addEventListener('click', (event) => AuthorTemplate.removeLine(event));
            });
        }
        if (inputCheckBox) {
            inputCheckBox.forEach((checkBox) => {
                checkBox.addEventListener('change', (event) => {
                    const target = event.target;
                    if (target.checked) {
                        target.value = 1;
                    } else {
                        target.value = 0;
                    }
                });
            })
        }
    }

    static addNewLine(event)
    {
        const clone = AuthorTemplate.getClone();
        const container = document.querySelector(AuthorTemplate.containerLineSelector);
        container.append(clone);
    }

    static removeLine(event)
    {
        const line = AuthorTemplate.getLineAuthor(event.target);
        line.remove()
    }

    static getLineAuthor(element)
    {
        while(element = element.parentElement) {
            if (element.classList.contains(AuthorTemplate.lineAuthorClass)) {
                return element;
            }
        }
    }

    static getClone()
    {
        const clone = document.querySelector(AuthorTemplate.lineAuthorCloneSelector).cloneNode(true);
        const addBtn = clone.querySelector(AuthorTemplate.btnAddLineSelector);
        const removeBtn = clone.querySelector(AuthorTemplate.btnRemoveLineSelector);
        clone.id = '';
        clone.classList.remove('hidden');
        addBtn.remove();
        removeBtn.addEventListener('click', (event) => AuthorTemplate.removeLine(event));
        removeBtn.classList.remove('hidden');

        return clone;
    }
}
