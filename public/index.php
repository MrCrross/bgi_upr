<?php

$backModules = [
    'admin',
    'api'
];
$link = explode('/', $_SERVER['REQUEST_URI']);
$link[1] = preg_replace('/\?(.*)/', '', $link)[1];

if (!in_array($link[1], $backModules)) {
    include_once('dist/index.html');
    exit();
}

use Illuminate\Http\Request;

define('LARAVEL_START', microtime(true));

// Determine if the application is in maintenance mode...
if (file_exists($maintenance = __DIR__.'/../storage/framework/maintenance.php')) {
    require $maintenance;
}

// Register the Composer autoloader...
require __DIR__.'/../vendor/autoload.php';

// Bootstrap Laravel and handle the request...
(require_once __DIR__.'/../bootstrap/app.php')
    ->handleRequest(Request::capture());
