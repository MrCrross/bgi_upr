<?php

namespace Database\Seeders;

use App\Models\Genre;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class GenresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $genres = [
            ['name' => 'Фантастика'],
            ['name' => 'Детектив'],
            ['name' => 'Роман'],
            ['name' => 'Приключения'],
            ['name' => 'Фэнтези'],
            ['name' => 'Научная фантастика'],
            ['name' => 'Триллер'],
            ['name' => 'Мистика'],
            ['name' => 'Биография'],
            ['name' => 'Поэзия']
        ];

        foreach ($genres as $genre) {
            Genre::query()->create($genre);
        }
    }
}
