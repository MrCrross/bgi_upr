<?php

namespace Database\Seeders;

use App\Models\Author;
use App\Models\Genre;
use Illuminate\Database\Seeder;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            OrderStatusSeeder::class,
            UserSeeder::class,
//            FactorySeeder::class,
            AuthorsSeeder::class,
            GenresSeeder::class,
            PublishersSeeder::class,
            ProductsSeeder::class,
            NewsSeeder::class,
        ]);
    }
}
