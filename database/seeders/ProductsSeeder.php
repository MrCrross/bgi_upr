<?php

namespace Database\Seeders;

use App\Models\Product;
use App\Models\ProductsAuthor;
use App\Models\ProductsGenre;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $products = [
            [
                "name" => "Война и мир",
                "image" => "/storage/upload/products/1/img.webp",
                "pages" => 1225,
                "price" => 1500,
                "year_published" => 1869,
                "publisher_id" => 1,
                "isbn" => "978-5-699-94727-0",
                "description" => "Эпический роман-эпопея, рассказывающий о войне и мире."
            ],
            [
                "name" => "Преступление и наказание",
                "image" => "/storage/upload/products/2/img.jpg",
                "pages" => 671,
                "price" => 900,
                "year_published" => 1866,
                "publisher_id" => 2,
                "isbn" => "978-5-699-94726-3",
                "description" => "Роман о моральных дилеммах бедного студента."
            ],
            [
                "name" => "Анна Каренина",
                "image" => "/storage/upload/products/3/img.jpg",
                "pages" => 864,
                "price" => 1200,
                "year_published" => 1878,
                "publisher_id" => 3,
                "isbn" => "978-5-699-94728-7",
                "description" => "История трагической любви в русском высшем обществе."
            ],
            [
                "name" => "Мастер и Маргарита",
                "image" => "/storage/upload/products/4/img.jpg",
                "pages" => 480,
                "price" => 950,
                "year_published" => 1967,
                "publisher_id" => 4,
                "isbn" => "978-5-699-94729-4",
                "description" => "Философско-мистический роман о Москве и Иерусалиме."
            ],
            [
                "name" => "Евгений Онегин",
                "image" => "/storage/upload/products/5/img.jpg",
                "pages" => 432,
                "price" => 750,
                "year_published" => 1833,
                "publisher_id" => 5,
                "isbn" => "978-5-699-94730-0",
                "description" => "Роман в стихах о жизни молодого дворянина."
            ],
            [
                "name" => "Мёртвые души",
                "image" => "/storage/upload/products/6/img.jpg",
                "pages" => 352,
                "price" => 800,
                "year_published" => 1842,
                "publisher_id" => 6,
                "isbn" => "978-5-699-94731-7",
                "description" => "Роман о путешествиях по России и торговле мёртвыми душами."
            ],
            [
                "name" => "Отцы и дети",
                "image" => "/storage/upload/products/7/img.jpg",
                "pages" => 320,
                "price" => 700,
                "year_published" => 1862,
                "publisher_id" => 7,
                "isbn" => "978-5-699-94732-4",
                "description" => "Роман о конфликте поколений в русской семье."
            ],
            [
                "name" => "Герой нашего времени",
                "image" => "/storage/upload/products/8/img.jpg",
                "pages" => 304,
                "price" => 650,
                "year_published" => 1840,
                "publisher_id" => 8,
                "isbn" => "978-5-699-94733-1",
                "description" => "Роман о жизни и судьбе молодого офицера."
            ],
            [
                "name" => "Двенадцать стульев",
                "image" => "/storage/upload/products/9/img.jpg",
                "pages" => 288,
                "price" => 600,
                "year_published" => 1928,
                "publisher_id" => 9,
                "isbn" => "978-5-699-94734-8",
                "description" => "Сатирический роман о поисках драгоценностей в Советской России."
            ],
            [
                "name" => "Лолита",
                "image" => "/storage/upload/products/10/img.jpg",
                "pages" => 336,
                "price" => 850,
                "year_published" => 1955,
                "publisher_id" => 10,
                "isbn" => "978-5-699-94735-5",
                "description" => "Провокационный роман о сложных взаимоотношениях."
            ]
        ];

        $productsAuthors = [
            [
                'product_id' => 1,
                'author_id' => 1,
            ],
            [
                'product_id' => 2,
                'author_id' => 2,
            ],
            [
                'product_id' => 3,
                'author_id' => 1,
            ],
            [
                'product_id' => 4,
                'author_id' => 7,
            ],
            [
                'product_id' => 5,
                'author_id' => 4,
            ],
            [
                'product_id' => 6,
                'author_id' => 5,
            ],
            [
                'product_id' => 7,
                'author_id' => 6,
            ],
            [
                'product_id' => 8,
                'author_id' => 8,
            ],
            [
                'product_id' => 9,
                'author_id' => 9,
            ],
            [
                'product_id' => 10,
                'author_id' => 10,
            ],
        ];

        $productsGenres = [
            [
                'product_id' => 1,
                'genre_id' => 1,
            ],
            [
                'product_id' => 1,
                'genre_id' => 3,
            ],
            [
                'product_id' => 2,
                'genre_id' => 2,
            ],
            [
                'product_id' => 2,
                'genre_id' => 3,
            ],
            [
                'product_id' => 3,
                'genre_id' => 3,
            ],
            [
                'product_id' => 4,
                'genre_id' => 1,
            ],
            [
                'product_id' => 4,
                'genre_id' => 8,
            ],
            [
                'product_id' => 5,
                'genre_id' => 3,
            ],
            [
                'product_id' => 5,
                'genre_id' => 10,
            ],
            [
                'product_id' => 6,
                'genre_id' => 1,
            ],
            [
                'product_id' => 6,
                'genre_id' => 3,
            ],
            [
                'product_id' => 7,
                'genre_id' => 3,
            ],
            [
                'product_id' => 8,
                'genre_id' => 3,
            ],
            [
                'product_id' => 9,
                'genre_id' => 1,
            ],
            [
                'product_id' => 9,
                'genre_id' => 4,
            ],
            [
                'product_id' => 10,
                'genre_id' => 3,
            ],
            [
                'product_id' => 10,
                'genre_id' => 7,
            ],
        ];

        foreach ($products as $product) {
            Product::query()->create($product);
        }
        foreach ($productsAuthors as $productAuthor) {
            ProductsAuthor::query()->insert($productAuthor);
        }
        foreach ($productsGenres as $productGenre) {
            ProductsGenre::query()->insert($productGenre);
        }
    }
}
