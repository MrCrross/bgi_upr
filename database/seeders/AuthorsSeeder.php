<?php

namespace Database\Seeders;

use App\Models\Author;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AuthorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $authors = [
            ['fio' => 'Толстой Лев Николаевич'],
            ['fio' => 'Достоевский Фёдор Михайлович'],
            ['fio' => 'Чехов Антон Павлович'],
            ['fio' => 'Пушкин Александр Сергеевич'],
            ['fio' => 'Гоголь Николай Васильевич'],
            ['fio' => 'Тургенев Иван Сергеевич'],
            ['fio' => 'Булгаков Михаил Афанасьевич'],
            ['fio' => 'Лермонтов Михаил Юрьевич'],
            ['fio' => 'Горький Максим Алексеевич'],
            ['fio' => 'Набоков Владимир Владимирович']
        ];

        foreach ($authors as $author) {
            Author::query()->create($author);
        }
    }
}
