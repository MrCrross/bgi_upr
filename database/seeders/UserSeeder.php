<?php

namespace Database\Seeders;

use App\Models\Users\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $permissions = [
            'users_view',
            'users_edit',
            'users_delete',
            'roles_view',
            'roles_edit',
            'roles_delete',
            'authors_edit',
            'authors_view',
            'genres_edit',
            'genres_view',
            'publishers_view',
            'publishers_edit',
            'files_view',
            'files_view-delete',
            'files_edit',
            'news_edit',
            'orders_view',
            'orders_view-delete',
            'orders_edit',
            'orders_create',
            'products_edit',
            'products_delete',
        ];
        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }

        $roles = [
            [
                'name' => 'root',
            ],
            [
                'name' => 'default',
            ],
        ];
        // role => permissions
        $rolesPermissions = [
            'root' => Permission::pluck('id','id')->all(),
            'default' => Permission::whereIn(
                'name',
                [
                    'orders_create'
                ]
            )
                ->pluck('id','id'),
        ];
        $rolesIDs = [];
        foreach ($roles as $role) {
            $newRole = Role::create($role);
            $rolesIDs[$role['name']] = $newRole->id;
            $newRole->syncPermissions($rolesPermissions[$role['name']]);
        }

        $users = [
            'root' => [
                'name' => 'Администратор',
                'password' => Hash::make('qweqwe123'),
                'email' => 'admin@bgi.com',
                'email_verified_at' => Carbon::now()->toDateTimeString(),
            ],
            'default' => [
                'name' => 'Юносов Павел Ревхатович',
                'password' => Hash::make('qweqwe123'),
                'email' => 'crrrya@yandex.ru',
                'email_verified_at' => Carbon::now()->toDateTimeString(),
            ],
        ];
        // login => role id
        $usersRoles = [
            'root' => $rolesIDs['root'],
            'default' => $rolesIDs['default'],
        ];
        foreach ($users as $role => $user) {
            $newUser = User::create($user);
            $newUser->assignRole($usersRoles[$role]);
        }
    }
}
