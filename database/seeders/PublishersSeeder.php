<?php

namespace Database\Seeders;

use App\Models\Publisher;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PublishersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $publishers = [
            ['name' => 'АСТ'],
            ['name' => 'Эксмо'],
            ['name' => 'Проспект'],
            ['name' => 'Молодая гвардия'],
            ['name' => 'Лениздат'],
            ['name' => 'Росмэн'],
            ['name' => 'Азбука-Аттикус'],
            ['name' => 'МИФ'],
            ['name' => 'Самокат'],
            ['name' => 'Альпина Паблишер']
        ];

        foreach ($publishers as $publisher) {
            Publisher::query()->create($publisher);
        }
    }
}
