<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\WelcomeController;
use App\Http\Middleware\JWTValidation;
use App\Http\Middleware\UserAuthorizationValidation;
use App\Http\Modules\Auth\AuthController;
use App\Http\Modules\Authors\AuthorsController;
use App\Http\Modules\Basket\BasketController;
use App\Http\Modules\Files\FilesController;
use App\Http\Modules\Genres\GenresController;
use App\Http\Modules\News\NewsController;
use App\Http\Modules\Orders\OrdersController;
use App\Http\Modules\Products\ProductsController;
use App\Http\Modules\Publishers\PublishersController;
use App\Http\Modules\Roles\RolesController;
use App\Http\Modules\Users\UsersController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::prefix('admin')->group(function () {
    Route::get('/', function () {
       if (Auth::check()) {
           return redirect()->route('profile.edit');
       } else {
           return redirect()->route('login');
       }
    });
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->middleware(['auth', 'verified'])->name('dashboard');

    Route::middleware('auth')->group(function () {
        Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
        Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
        Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

        Route::resource('roles', RolesController::class);

        Route::resource('users', UsersController::class);
        Route::patch('/users/{id}/recovery', [UsersController::class, 'recovery'])->name('users.recovery');

        Route::resource('authors', AuthorsController::class);
        Route::patch('/authors/{id}/recovery', [AuthorsController::class, 'recovery'])->name('authors.recovery');

        Route::resource('genres', GenresController::class);
        Route::patch('/genres/{id}/recovery', [GenresController::class, 'recovery'])->name('genres.recovery');

        Route::resource('publishers', PublishersController::class);
        Route::patch('/publishers/{id}/recovery', [PublishersController::class, 'recovery'])->name('publishers.recovery');

        Route::resource('orders', OrdersController::class);
        Route::patch('/orders/{id}/recovery', [OrdersController::class, 'recovery'])->whereNumber('id')->name('orders.recovery');
        Route::post('/orders/{id}/repeat', [OrdersController::class, 'repeat'])->whereNumber('id')->name('orders.repeat');
        Route::post('/orders/{id}/cancel', [OrdersController::class, 'cancel'])->whereNumber('id')->name('orders.cancel');

        Route::resource('files', FilesController::class);
        Route::patch('/files/{id}/recovery', [FilesController::class, 'recovery'])->name('files.recovery');
        // PRODUCTS
        Route::get('/products', [ProductsController::class, 'adminIndex'])->name('products.admin.index');
        Route::get('/products/{id}', [ProductsController::class, 'adminShow'])->whereNumber('id')->name('products.admin.show');
        Route::get('/products/{id}/edit', [ProductsController::class, 'adminEdit'])->whereNumber('id')->name('products.admin.edit');
        Route::get('/products/create', [ProductsController::class, 'adminCreate'])->name('products.admin.create');
        Route::post('/products', [ProductsController::class, 'adminStore'])->name('products.admin.store');
        Route::patch('/products/{id}', [ProductsController::class, 'adminUpdate'])->whereNumber('id')->name('products.admin.update');
        Route::delete('/products/{id}', [ProductsController::class, 'adminDestroy'])->whereNumber('id')->name('products.admin.destroy');
        Route::patch('/products/{id}/recovery', [ProductsController::class, 'adminRecovery'])->whereNumber('id')->name('products.admin.recovery');
        // NEWS
        Route::get('/news', [NewsController::class, 'adminIndex'])->name('news.admin.index');
        Route::get('/news/{id}', [NewsController::class, 'adminShow'])->whereNumber('id')->name('news.admin.show');
        Route::get('/news/{id}/edit', [NewsController::class, 'adminEdit'])->whereNumber('id')->name('news.admin.edit');
        Route::get('/news/create', [NewsController::class, 'adminCreate'])->name('news.admin.create');
        Route::post('/news', [NewsController::class, 'adminStore'])->name('news.admin.store');
        Route::patch('/news/{id}', [NewsController::class, 'adminUpdate'])->whereNumber('id')->name('news.admin.update');
        Route::delete('/news/{id}', [NewsController::class, 'adminDestroy'])->whereNumber('id')->name('news.admin.destroy');
        Route::patch('/news/{id}/recovery', [NewsController::class, 'adminRecovery'])->whereNumber('id')->name('news.admin.recovery');
    });

    require __DIR__.'/auth.php';
});

Route::prefix('api')->group(function () {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/registration', [AuthController::class, 'registration']);
    Route::middleware([
        JWTValidation::class,
        UserAuthorizationValidation::class
    ])->group(function () {
        Route::get('/user', [AuthController::class, 'getCurrentUser']);
        Route::post('/logout', [AuthController::class, 'logout']);
        Route::post('/news', [NewsController::class, 'store']);
        Route::patch('/news/{id}', [NewsController::class, 'update'])->whereNumber('id');
        Route::delete('/news/{id}', [NewsController::class, 'destroy'])->whereNumber('id');
        Route::post('/news/{id}/recovery', [NewsController::class, 'recovery'])->name('news.recovery');
        Route::post('/products', [ProductsController::class, 'store']);
        Route::patch('/products/{id}', [ProductsController::class, 'update'])->whereNumber('id');
        Route::delete('/products/{id}', [ProductsController::class, 'destroy'])->whereNumber('id');
        Route::post('/products/{id}/recovery', [ProductsController::class, 'recovery']);
        Route::get('/basket', [BasketController::class, 'index'])->name('basket.index');
        Route::post('/products/{id}/basket', [BasketController::class, 'store'])->whereNumber('id')->name('basket.store');
        Route::patch('/basket', [BasketController::class, 'update'])->name('basket.update');
        Route::post('/basket/order', [OrdersController::class, 'storeByBasket']);
    });
    Route::get('/welcome', [WelcomeController::class, 'index']);
    Route::get('/products', [ProductsController::class, 'index']);
    Route::get('/products/{id}', [ProductsController::class, 'show'])->whereNumber('id');
    Route::get('/news', [NewsController::class, 'index']);
    Route::get('/news/{id}', [NewsController::class, 'show'])->whereNumber('id');
    Route::get('/autocomplete/authors', [AuthorsController::class, 'autocomplete']);
    Route::get('/autocomplete/genres', [GenresController::class, 'autocomplete']);
    Route::get('/autocomplete/publishers', [PublishersController::class, 'autocomplete']);
    Route::get('/autocomplete/users', [UsersController::class, 'autocomplete']);
});


